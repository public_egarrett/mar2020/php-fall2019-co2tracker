<?php
use Illuminate\Database\Seeder;
use App\Location;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::create(['latitude'=>45.430142, 'longitude'=>-73.622743]); //45.430142,-73.622743
        Location::create(['latitude'=>45.531519, 'longitude'=>-73.639131]); //45.531519,-73.639131
        Location::create(['latitude'=>45.571646, 'longitude'=>-73.582496]); //45.571646,-73.582496
        Location::create(['latitude'=>45.490131, 'longitude'=>-73.589383]); //Dawson 45.490131,-73.589383
        
        Location::create(['latitude'=>45.544898, 'longitude'=>-73.632378]); //45.544898,-73.632378
        Location::create(['latitude'=>45.592102, 'longitude'=>-73.645636]); //45.592102, -73.645636
        Location::create(['latitude'=>45.664724, 'longitude'=>-73.546579]);
        Location::create(['latitude'=>45.637071, 'longitude'=>-73.495339]); //45.637071,-73.495339
        
        Location::create(['latitude'=>45.423083, 'longitude'=>-73.626669]); //45.423083,-73.626669
        Location::create(['latitude'=>45.444473, 'longitude'=>-73.767037]);
        Location::create(['latitude'=>45.460008, 'longitude'=>-73.822495]); //45.460008,-73.822495
        Location::create(['latitude'=>45.474160, 'longitude'=>-73.867564]); //45.474160,-73.867564
        
        Location::create(['latitude'=>45.459865, 'longitude'=>-73.893447]); //45.459865,-73.893447
        Location::create(['latitude'=>45.446578, 'longitude'=>-73.919609]); //45.446578,-73.919609
        Location::create(['latitude'=>45.405560, 'longitude'=>-73.948022]); //45.405560,-73.948022
        Location::create(['latitude'=>45.416809, 'longitude'=>-74.011469]); //45.416809,-74.011469
    }
}
