<?php

use Illuminate\Database\Seeder;
use App\VehicleType;

class VehicleTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VehicleType::create(['id'=> 1, 'type'=> 'gasoline']);
        VehicleType::create(['id'=> 2,'type'=>'diesel']);
        VehicleType::create(['id'=> 3,'type'=>'electric']);
    }
}
