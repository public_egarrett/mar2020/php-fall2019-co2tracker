<?php

use Illuminate\Database\Seeder;
use App\Vehicle;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vehicle::create(['vehicle_type_id'=> 1, 'avg_consumption'=>5.5]);
        Vehicle::create(['vehicle_type_id'=> 1, 'avg_consumption'=>6.8]);
        Vehicle::create(['vehicle_type_id'=> 2, 'avg_consumption'=>5]);
        Vehicle::create(['vehicle_type_id'=> 3]); //electric car doesnt emit co2
    }
}
