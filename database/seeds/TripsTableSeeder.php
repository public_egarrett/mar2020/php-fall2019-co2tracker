<?php

use Illuminate\Database\Seeder;
use App\Trip;

class TripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        //user 1
        Trip::create(['user_id'=>1, 'start_location_id'=>4, 
                            'destination_location_id'=>2,
                            'travel_duration'=>3197,
                            'travel_distance_km'=>11.708, 
                            'emission_vol'=>0.05409096, 
                             'transport_id'=>3]);
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>2, 
                            'destination_location_id'=>10,
                            'travel_duration'=>4207, 
                            'travel_distance_km'=>19.344, 
                            'emission_vol'=>0.08936928, 
                             'transport_id'=>3]);
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>10, 
                           'destination_location_id'=>2,
                            'travel_duration'=>5080, 
                            'travel_distance_km'=>26.091, 
                            'emission_vol'=>0.12054042, 
                             'transport_id'=>3]);
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>4, 
                           'destination_location_id'=>9,
                            'travel_duration'=>3608, 
                            'travel_distance_km'=>16.183, 
                            'emission_vol'=>0.07476546, 
                             'transport_id'=>3]);
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>9, 
                            'destination_location_id'=>6,
                            'travel_duration'=>6021, 
                            'travel_distance_km'=>30.408, 
                            'emission_vol'=>0.14048496, 
                             'transport_id'=>3]);
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>3, 
                           'destination_location_id'=>4,
                            'travel_duration'=>11267, 
                            'travel_distance_km'=>11.244, 
                            'emission_vol'=>0.05194728, 
                             'transport_id'=>5]);
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>2, 
                           'destination_location_id'=>8,
                            'travel_duration'=>4861, 
                            'travel_distance_km'=>18.837, 
                            'emission_vol'=>0.08702694, 
                             'transport_id'=>4]);
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>2, 
                           'destination_location_id'=>4,
                            'travel_duration'=>1284,
                            'travel_distance_km'=>8.915, 
                            'emission_vol'=>0.571,
                             'transport_id'=>2, 'vehicle_id' => 1]); //carpool with user 1
                             
        Trip::create(['user_id'=>1, 'start_location_id'=>1, 
                           'destination_location_id'=>14,
                            'travel_duration'=>1983,
                            'travel_distance_km'=>28.410, 
                            'emission_vol'=>1.8205, 
                             'transport_id'=>2, 'vehicle_id' => 1]); //carpool with user 1
                             
        
        
        //user 2                     
        Trip::create(['user_id'=>2, 'start_location_id'=>1, 
                            'destination_location_id'=>5,
                            'travel_duration'=>1512,
                            'travel_distance_km'=>20.668, 
                            'emission_vol'=>2.649, 
                             'transport_id'=>1, 
                             'vehicle_id' => 1]);
                             
        Trip::create(['user_id'=>2, 'start_location_id'=>11, 
                           'destination_location_id'=>1,
                            'travel_duration'=>1484,
                            'travel_distance_km'=>19.880, 
                            'emission_vol'=>2.548, 
                             'transport_id'=>1, 
                             'vehicle_id' => 1]);
                             
        Trip::create(['user_id'=>2, 'start_location_id'=>12, 
                           'destination_location_id'=>1,
                            'travel_duration'=>1463,
                            'travel_distance_km'=>19.739, 
                            'emission_vol'=>2.53, 
                             'transport_id'=>1, 'vehicle_id' => 1]);
                             
        Trip::create(['user_id'=>2, 'start_location_id'=>13, 
                           'destination_location_id'=>4,
                            'travel_duration'=>1920,
                            'travel_distance_km'=>31.692, 
                            'emission_vol'=>4.061, 
                             'transport_id'=>1, 'vehicle_id' => 1]);
                             
        Trip::create(['user_id'=>2, 'start_location_id'=>2, 
                            'destination_location_id'=>4,
                            'travel_duration'=>1284,
                            'travel_distance_km'=>8.915, 
                            'emission_vol'=>0.571,
                             'transport_id'=>2, 'vehicle_id' => 1]); //carpool with user 2
                             
        Trip::create(['user_id'=>2, 'start_location_id'=>1, 
                           'destination_location_id'=>14,
                            'travel_duration'=>1983,
                            'travel_distance_km'=>28.410, 
                            'emission_vol'=>1.8205, 
                             'transport_id'=>2, 
                             'vehicle_id' => 1]); //carpool with user 2
                             
        Trip::create(['user_id'=>2, 'start_location_id'=>8, 
                            'destination_location_id'=>7,
                            'travel_duration'=>6004,
                            'travel_distance_km'=>5.953, 
                            'emission_vol'=>0, 
                             'transport_id'=>5]);
                             
        Trip::create(['user_id'=>2, 'start_location_id'=>7, 
                           'destination_location_id'=>8,
                            'travel_duration'=>6012,
                            'travel_distance_km'=>5.953, 
                            'emission_vol'=>0, 
                             'transport_id'=>5]);       
                             
                             
        //user 3                     
        Trip::create(['user_id'=>3, 'start_location_id'=>15, 
                           'destination_location_id'=>16,
                            'travel_duration'=>10842,
                            'travel_distance_km'=>10.694, 
                            'emission_vol'=>0, 
                             'transport_id'=>5]);
        Trip::create(['user_id'=>3, 'start_location_id'=>14, 
                           'destination_location_id'=>15,
                            'travel_duration'=>8317,
                            'travel_distance_km'=>8.231, 
                            'emission_vol'=>0, 
                             'transport_id'=>5]);
                             
        
        //user 4                     
        Trip::create(['user_id'=>4, 'start_location_id'=>8, 
                           'destination_location_id'=>7,
                            'travel_duration'=>6004,
                            'travel_distance_km'=>5.953, 
                            'emission_vol'=>0, 
                             'transport_id'=>5]);
        Trip::create(['user_id'=>4, 'start_location_id'=>7, 
                           'destination_location_id'=>8,
                            'travel_duration'=>6012,
                            'travel_distance_km'=>5.953, 
                            'emission_vol'=>0, 
                             'transport_id'=>5]);         
    }
}
