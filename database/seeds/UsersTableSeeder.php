<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Hash for the password 'password'
        User::create(['name'=>'eira', 'email'=>'e@foxes.com', 'password'=>'$2y$10$a6Y5OfHhClGqbkG6yL.PPeedVDzQpbv4CxrLJ4oQ4C3MwUme82KMm']);
        User::create(['name'=>'camillia', 'email'=>'c@foxes.com', 'password'=>'$2y$10$a6Y5OfHhClGqbkG6yL.PPeedVDzQpbv4CxrLJ4oQ4C3MwUme82KMm']);
        User::create(['name'=>'rhobza', 'email'=>'r@kitties.com', 'password'=>'$2y$10$a6Y5OfHhClGqbkG6yL.PPeedVDzQpbv4CxrLJ4oQ4C3MwUme82KMm']);
        User::create(['name'=>'lulu', 'email'=>'l@kitties.com', 'password'=>'$2y$10$a6Y5OfHhClGqbkG6yL.PPeedVDzQpbv4CxrLJ4oQ4C3MwUme82KMm']);
    }
}
