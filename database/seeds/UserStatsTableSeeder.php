<?php

use Illuminate\Database\Seeder;
use App\UserStat;

class UserStatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserStat::create(['user_id'=> 1, 'home_location_id'=>2, 
                            'total_commute_km'=>171.14,
                            'yearly_emissions'=>3.0097253,
                            'offset'=>0.09
                            ]);
                            
        UserStat::create(['user_id'=> 2, 'home_location_id'=>1, 
                            'vehicle_id'=>1, 
                            'total_commute_km'=>141.21,
                            'yearly_emissions'=>14.1795,
                            'offset'=>0.43
                            ]);
                            
        UserStat::create(['user_id'=> 3, 
                            'home_location_id'=>15, 
                            'vehicle_id'=>2,
                            'total_commute_km'=>18.925,
                            'yearly_emissions'=>0,
                            'offset'=>0
                            ]);
          
        UserStat::create(['user_id'=> 4, 
                        'home_location_id'=>8,
                            'total_commute_km'=>11.906,
                            'yearly_emissions'=>0,
                            'offset'=>0
                            ]);
    }
}
