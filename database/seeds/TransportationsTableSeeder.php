<?php

use Illuminate\Database\Seeder;
use App\Transportation;

class TransportationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Transportation::create(['id'=> 1, 'transportation_method'=>'car']);
       Transportation::create(['id'=> 2, 'transportation_method'=>'carpool']);
       Transportation::create(['id'=> 3, 'transportation_method'=>'transit']);
       Transportation::create(['id'=> 4, 'transportation_method'=>'bike']);
       Transportation::create(['id'=> 5, 'transportation_method'=>'walk']);
    }
}
