<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VehicleTypesTableSeeder::class);
        $this->call(VehiclesTableSeeder::class);
        $this->call(TransportationsTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserStatsTableSeeder::class);
        $this->call(TripsTableSeeder::class);
    }
}
