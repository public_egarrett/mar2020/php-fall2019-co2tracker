<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vehicle_type_id')->unsigned()->index();
            $table->double('avg_consumption')->default(0);
            $table->timestamps();
            
            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_types')->onDelete('cascade');
        });
        
        //If the the car is not electric, an avg consumption needs to be specified
        DB::statement('ALTER TABLE vehicles ADD CONSTRAINT chk_consumption '.
            'CHECK ((vehicle_type_id IN (1, 2) AND avg_consumption > 0) OR vehicle_type_id NOT IN (1,2));');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
