<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('user_id')->unsigned()->index();
            
            $table->bigInteger('start_location_id')->unsigned()->index();
            $table->bigInteger('destination_location_id')->unsigned()->index();
            $table->bigInteger('transport_id')->unsigned()->index();
            $table->bigInteger('vehicle_id')->unsigned()->index()->nullable();
            
            $table->double('travel_duration')->default(0);
            $table->double('travel_distance_km')->default(0);
            $table->double('emission_vol')->default(0);
            
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            
            $table->foreign('start_location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('destination_location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('transport_id')->references('id')->on('transportations')->onDelete('cascade');
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
        });
        
        //If the transport id is referring to car or carpool, vehicle_id should not be null
        DB::statement('ALTER TABLE trips ADD CONSTRAINT chk_transport_vehicle '.
            'CHECK ((transport_id IN (1, 2) AND vehicle_id IS NOT NULL) OR transport_id NOT IN (1,2));');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
