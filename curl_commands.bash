//Authentication
curl -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"email": "demo@co2tracker.com", "password":"password"}' https://co2trackerbyec.herokuapp.com/api/auth/login

//Get All trips
curl -X GET -H "Accept: application/json" -H "Authorization: Bearer bearer_token_here"  https://co2trackerbyec.herokuapp.com/api/v1/alltrips

//Get trip info
curl -X GET -H "Accept: application/json" -H "Authorization: Bearer bearer_token_here" "https://co2trackerbyec.herokuapp.com/api/v1/alltrips?tolatitude=45.544898&tolongitude=-73.632378&fromlatitude=45.637071&fromlongitude=-73.495339&mode=transit"

//Add new trip
curl -X POST -H "Accept: application/json" -H "Authorization: Bearer bearer_token_here" -H "Content-Type: application/json" http://co2trackerbyec.herokuapp.com/api/v1/addtrip -d '{"fromlatitude":45.544898, "fromlongitude":-73.632378,"tolatitude":45.637071,  "tolongitude":-73.495339,"mode":"publicTransport"}'