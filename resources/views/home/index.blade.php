@extends('layouts.app')

@section('content')
<?php use App\Location; ?>
    <div class="container" >
        <div class="col-sm-offset-2 col-sm-8" style="margin: auto;">
            @if (Auth::check())
                <!-- Display personalized welcome -->
                <div class="panel-heading">
                    Welcome, <strong>{{ucfirst(Auth::user()->name)}} </strong>! You have been a member since {{ Auth::user()->created_at }}.<br/><br/>
                    
                    <h3 style="color:#2d4302">Your Total Travel Distance:</h3>
                    <p><strong>{{round(Auth::user()->userstat->total_commute_km, 1)}}</strong> km </p>
                    
                    <h3 style="color:#2d4302">Your Emissions:</h3>
                    <p><strong>{{round(Auth::user()->userstat->yearly_emissions,1)}}</strong> kg of CO<sub>2</sub> this year </p>
                    
                    <h3 style="color:#2d4302">Your Offset Cost:</h3>
                    <p><strong>{{round(Auth::user()->userstat->offset, 2)}}</strong>$ this year</p>
                </div>
            @else
                <!-- Display general welcome -->
                <div class="panel-heading" style="text-align: center"> 
                <h1>GOOD people care about our planet.</h3>
                <h2>That's why YOU are here!</h2>
                <img src="https://gallery.yopriceville.com/var/resizes/Free-Clipart-Pictures/Trees-PNG-Clipart/Green_Heart_Tree_Transparent_PNG_Image.png?m=1507172102" height="300">
                <p>This Co2 Emissions Tracker will help you make better travel choices, 
                by allowing you to track your trips and CO2 emissions. You will 
                also be able to understand the different options that exist for 
                travelling and their respective impact on the environment. </p>
                <h2>Sign up <strong>today</strong> for a better tomorrow.</h2>
            @endif
            
            <!-- New trip form -->
            @if (Auth::check())
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 style="color:#2d4302">New Trip</h3>
                    </div>
                    
                    <div class="panel-body">
                        <!--display validation errors-->
                        @include('common.errors')
                        
                        <!-- New trip form body -->
                        <form action="/home/routeselect" method="POST" class="form-horizontal">
                            {{ csrf_field() }}
                            
                            <!-- Trip start address -->
                            <div class="form-group">
                                <label for="start-address" class="col-sm-3 control-label">{{__('Start Address')}}</label>
                                <div class="col-sm-6">
                                    <select name="start-address" id="start-dropdown" class="form-control" required>
                                        <option value="home" selected="selected">Home</option>
                                        <option value="dawson">Dawson</option>
                                        <option value="new" id="start-new">New</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Will create new textbox if 'new' is selected -->
                            <div id="new_start_address_div"></div>
                            
                            <!-- Trip destination address-->
                            <div class="form-group">
                                <label for="destination-address" class="col-sm-3 control-label">{{__('Destination Address')}}</label>
                                <div class="col-sm-6">
                                    <select id="destination-dropdown" name="destination-address" class="form-control" required>
                                        <option value="home">Home</option>
                                        <option value="dawson" selected="selected">Dawson</option>
                                        <option value="new" id="destination-new">New</option>
                                    </select>
                                </div>
                            </div>
                            <div id="new_destination_address_div"></div>
                            
                            <!--Show Routes Button-->
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" id="routebtn">
                                        {{ __('Show Routes') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <br/><br/>
                
                <!-- Show all trips associated with the current user -->
                <div >
                    @if (count($trips) > 0)
                        <div class="panel-body">
                            <table class="table table-striped task-table" style="background-color: rgba(255,255,255,.3);">
                                <thead>
                                    <th>Trip</th>
                                    <th>Date</th>
                                    <th>Travel Time</th>
                                    <th>Travel Distance</th>
                                    <th>Method of Transport</th>
                                    <th>Emissions Generated</th>
                                </thead>
                                <tbody>
                                    @foreach ($trips as $trip)
                                        @if($trip->userCanView(Auth::user()))
                                            <tr>
                                                <?php
                                                    $start = Location::where('id', $trip->start_location_id)->first();
                                                    $end = Location::where('id', $trip->destination_location_id)->first();
                                                    $dawson['latitude'] = 45.48775;
                                                    $dawson['longitude'] = -73.58854;
                                                ?>
                                                <td class = "table-text">
                                                    @if ($start->id == $home->id)
                                                        @if ($end->longitude == $dawson['longitude'] && $end->latitude == $dawson['latitude'])
                                                            {{__('Home to Dawson')}}
                                                        @else
                                                            {{__('Home to Other')}}
                                                        @endif
                                                    @elseif ($start->longitude == $dawson['longitude'] && $start->latitude == $dawson['latitude'])
                                                        @if ($end->id == $home->id)
                                                            {{__('Dawson to Home')}}
                                                        @else
                                                            {{__('Dawson to Other')}}
                                                        @endif
                                                    @else
                                                        @if ($end->id == $home->id)
                                                            {{__('Other to Home')}}
                                                        @elseif($end->longitude == $dawson['longitude'] && $end->latitude == $dawson['latitude'])
                                                            {{__('Other to Dawson')}}
                                                        @else
                                                            {{__('Other to Other')}}
                                                        @endif
                                                    @endif
                                                </td>
                                                <td class="table-text">
                                                    {{ $trip->created_at->setTimezone(new DateTimeZone('America/New_York')) }}
                                                </td>
                                                <td class="table-text">
                                                    {{ date('H:i:s', $trip->travel_duration) }}
                                                </td>
                                                <td class="table-text">
                                                    {{ round($trip->travel_distance_km, 2) }} km
                                                </td>
                                                <td class="table-text">
                                                    {{ ucfirst($transportations[$trip->transport_id])}}
                                                    
                                                </td>
                                                <td class="table-text">
                                                    {{ round($trip->emission_vol, 5) }} kg CO<sub>2</sub>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <div style="margin: auto; display:inline-block;">{{ $trips->links() }}</div>
                        </div>
                    @endif
                </div>
            @endif
        </div>
    </div>
    
    <script type="text/javascript">
        function addEvent(obj, type, fn) {
            if (obj && obj.addEventListener) {
                obj.addEventListener(type, fn, false);
            } else if (obj && obj.attachEvent) {
                obj.attachEvent('on' + type, fn);
            }
        }
        
        addEvent(document, 'DOMContentLoaded', init);
         
        function init() {
            var startDropDown = document.getElementById("start-dropdown");
            var destinationDropDown = document.getElementById("destination-dropdown");
            
            if (startDropDown != null) {
                addEvent(startDropDown, 'change', startSelectionChanged);
            }
            
            if (destinationDropDown != null) {
                addEvent(destinationDropDown, 'change', destinationSelectionChanged)
            }
        }
        
        /**
        * Adds or removes new input fields based on dropdown selection.
        */
        function startSelectionChanged() {
            var startDropDown = document.getElementById("start-dropdown");
            
            if (startDropDown != null) {
                var startSelectedElement = startDropDown.selectedIndex;
                
                var html = '<div class="form-group row">'
                            +'<label for="new-start-streetnum" class="col-sm-3 control-label">Street #</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-start-streetnum" id="new-start-streetnum" required/>'
                            +'</div>'
                        +'</div>'
                        +'<div class="form-group row">'
                            +'<label for="new-start-street" class="col-sm-3 control-label">Street</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-start-street" id="new-start-street" required/>'
                            +'</div>'
                        +'</div>'
                        +'<div class="form-group row">'
                            +'<label for="new-start-city" class="col-sm-3 control-label">City</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-start-city" id="new-start-city" required/>'
                            +'</div>'
                        +'</div>'
                        +'<div class="form-group row">'
                            +'<label for="new-start-country" class="col-sm-3 control-label">Country</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-start-country" id="new-start-country" required/>'
                            +'</div>'
                        +'</div>';
                
                var addressDiv = document.getElementById("new_start_address_div");
                
                if(startSelectedElement != null && startSelectedElement == 2) {
                    addressDiv.innerHTML = html;
                } else {
                    addressDiv.innerHTML = "";
                }
            }
        }
        
        /**
         * Generates new input fields for entering a new address.
         */
        function destinationSelectionChanged() {
            var destinationDropDown = document.getElementById("destination-dropdown");
            
            if(destinationDropDown != null) {
                var destinationSelectedElement = destinationDropDown.selectedIndex;
                
                var html = '<div class="form-group row">'
                            +'<label for="new-destination-streetnum" class="col-sm-3 control-label">Street #</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-destination-streetnum" id="new-destination-streetnum" required/>'
                            +'</div>'
                        +'</div>'
                        +'<div class="form-group row">'
                            +'<label for="new-destination-street" class="col-sm-3 control-label">Street</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-destination-street" id="new-destination-street" required/>'
                            +'</div>'
                        +'</div>'
                        +'<div class="form-group row">'
                            +'<label for="new-destination-city" class="col-sm-3 control-label">City</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-destination-city" id="new-destination-city" required/>'
                            +'</div>'
                        +'</div>'
                        +'<div class="form-group row">'
                            +'<label for="new-destination-country" class="col-sm-3 control-label">Country</label>'
                            +'<div class="col-md-6">'
                                +'<input type="text" class="form-control" name="new-destination-country" id="new-destination-country" required/>'
                            +'</div>'
                        +'</div>';
                var addressDiv = document.getElementById("new_destination_address_div");
                
                if(destinationSelectedElement != null && destinationSelectedElement == 2) {
                    addressDiv.innerHTML = html;
                } else {
                    addressDiv.innerHTML = "";
                }
            }
        }
    </script>
@endsection