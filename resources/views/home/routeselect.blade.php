@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                Trip Options:
            </div>
            
            <div class="panel-body">
                @include('common.errors')
                
                <form action="/home/newtrip" method="POST" class="form-horizontal">
                    {{csrf_field()}}
                    <table class="table table-striped task-table">
                        <thead>
                            <th>Transport Method</th>
                            <th>Distance</th>
                            <th>Travel Time</th>
                            <th>CO2 Emitted (kg)</th>
                            <th>Use this route?</th>
                        </thead>
                        <tbody>
                            @foreach($routes as $type => $route)
                            <tr>
                                <td class="table-text">{{ $type }}</td>
                                <td class="table-text">{{ round(($route['distance']/1000), 2) }} km</td>
                                <td class="table-text">{{ date('H:i:s', $route['travelTime']) }}</td>
                                @if ($type === 'Car')
                                    <td class="table-text"> {{ round($route['co2Emission'], 2) }} kg CO<sub>2</sub></td>
                                @elseif ($type === 'Carpool')
                                    <td class="table-text"> <div id="emissionval"></div> kg CO<sub>2</sub> / <select id="numpassengers" name="numpassengers" class="form-control">
                                        <option value="2">2 passengers</option>
                                        <option value="3">3 passengers</option>
                                        <option value="4">4 passengers</option>
                                        <option value="5">5 passengers</option>
                                        <option value="6">6 passengers</option>
                                        <option value="7">7 passengers</option>
                                        <option value="8">8 passengers</option>
                                    </select></td>
                                    
                                @elseif ($type === 'Transit')
                                    <td class="table-text"> {{ round(($route['distance'] / 1000 * 46.2), 2) }} g </td>
                                @else
                                    <td class="table-text"> 0 kg CO<sub>2</sub></td>
                                @endif
                                <td class="table-text">
                                    @if (array_key_exists('Car', $routes) && $type === 'Car')
                                        <input type="radio" name="method" value="{{ $type }}" checked>
                                    @elseif (!array_key_exists('Car', $routes) && $type === 'Transit')
                                        <input type="radio" name="method" value="{{ $type }}" checked>
                                    @else
                                        <input type="radio" name="method" value="{{ $type }}">
                                    @endif
                                    <!-- Hidden fields for storing trip information -->
                                    <input type="hidden" name="start_location" value="{{ $start }}">
                                    <input type="hidden" name="end_location" value="{{ $end }}">
                                    <input type="hidden" name="">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <button type="submit" class="btn btn-primary">
                        {{__('Submit')}}
                    </button>
                </form>
            </div>
        </div>
        
         <script type="text/javascript">
            function addEvent(obj, type, fn) {
                if (obj && obj.addEventListener) {
                    obj.addEventListener(type, fn, false);
                } else if (obj && obj.attachEvent) {
                    obj.attachEvent('on' + type, fn);
                }
            }
                                            
            addEvent(document, 'DOMContentLoaded', init);
                                            
            function init() {
                var passengerSelect = document.getElementById('numpassengers');
                
                passengerSelect.selectedElement = 0;
                
                var resultDiv = document.getElementById('emissionval');
                
                var baseEmissions = parseFloat("<?php if (array_key_exists('Carpool', $routes)) {echo json_encode($routes['Carpool']['co2Emission']);} ?>");
                
                var selectedElement = parseInt(passengerSelect.value);
                
                var value = baseEmissions / selectedElement;
                
                resultDiv.innerHTML = value;
                
                if (passengerSelect != null && baseEmissions != null) {
                    addEvent(passengerSelect, 'change', passengersChanged);
                }
            }
            
            function passengersChanged() {
                var passengerSelect = document.getElementById('numpassengers');
                var resultDiv = document.getElementById('emissionval');
                
                var baseEmissions = parseFloat("<?php if (array_key_exists('Carpool', $routes)) {echo json_encode($routes['Carpool']['co2Emission']);} ?>");
                
                if (passengerSelect != null && baseEmissions!= null) {
                    var selectedElement = parseInt(passengerSelect.value);
                    
                    var value = baseEmissions / selectedElement;
                    
                    resultDiv.innerHTML= value.toFixed(3);
                }
            }
        </script>

    </div>
@endsection