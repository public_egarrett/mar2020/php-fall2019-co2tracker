@extends('layouts.app')

@section('content')
    <div class="alert alert-danger">
        <strong>Address could not be found! Please try again.  </strong>
        <button class="btn btn-primary" type="submit" onclick="event.preventDefault(); document.getElementById('return-form').submit();">
            Go Back
        </button>
        <form id="return-form" action="/" method="GET" style="display:none;">@csrf</form>
    </div>
@endsection