<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


URL::forceScheme('https');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::get('/', 'HomeController@index');

Route::get('/home/routeselect', 'RouteSelectController@index');
Route::post('/home/routeselect', 'RouteSelectController@findroutes');
Route::post('/home/newtrip', 'RouteSelectController@store');
Route::post('/home', 'HomeController@store')->middleware('auth');
Route::delete('/home/{trip}', 'HomeController@destroy')->middleware('auth');
Route::get('/home/{trip}', 'HomeController@index');