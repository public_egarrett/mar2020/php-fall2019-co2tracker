<?php
namespace App;

class Constants {
    
    //Latitude values range
    public const LATITUDE_MIN_VALUE = -90.0;
    public const LATITUDE_MAX_VALUE = 90.0;
    
    //Longitude values range
    public const LONGITUDE_MIN_VALUE = -180.0;
    public const LONGITUDE_MAX_VALUE = 180.0;
    
    //Possible values for vehicle types
    public const VEHICLE_TYPES_LOWERCASE = ['none', 'diesel', 'gasoline', 'electric'];
    
    //Vehicle types which do not need a specified consumption
    public const VEHICLE_TYPES_LOWERCASE_NO_CONSUMPTION = ['electric'];
    
    //Possible values for transportation mode in API
    public const TRANSPORTATION_MODES_LOWERCASE = ['car', 'carpool', 'transit', 'bike', 'walk'];
    
    //Possible values for transportation mode in API for user with no car
    public const TRANSPORTATION_MODES_LOWERCASE_NO_CAR = ['transit', 'bike', 'walk'];
    
    //Possible values for transportation mode in API which are car-related
    public const TRANSPORTATION_MODES_LOWERCASE_CAR = ['car', 'carpool'];

    //Division factor to get the emissions per trip per user when the mode is 'carpool'
    public const CARPOOL_EMISSION_DIVISION_FACTOR = 3.0;
    
    //Multiplier factor to get the emissions per trip when the mode is 'transit'
    public const TRANSIT_EMISSIONS_KG_PER_KM = 0.0462;
    
    //Mutliplication factor to get offset from kg of emissions
    public const OFFSET_PRICE_PER_KG = 0.03;
    
    public const DAWSON_LATITUDE = 45.48775 ;
    
    public const DAWSON_LONGITUDE = -73.58854;
}