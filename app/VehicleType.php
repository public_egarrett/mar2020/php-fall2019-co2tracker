<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    /**
     * Mass-assignable attributes.
     * 
     * @var array
     */
    protected $fillable = ['type'];
    
    /**
     * Get the Vehicle associated with the vehicle type.
     */
    public function vehicle() {
        return $this->hasMany(Vehicle::class);
    }
    
    public function type(){
        return $this->type;
    }
}
