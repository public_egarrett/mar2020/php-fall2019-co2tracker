<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
    /**
     * Mass-assignable attributes.
     * 
     * @var array
     */
    protected $fillable = ['transportation_method'];
    
    /**
     * Get the trips associated with the transportation method.
     */
    public function trips() {
        return $this->hasMany(Trip::class);
    }
}