<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * Mass-assignable attributes.
     * 
     * @var array
     */
    protected $fillable = ['vehicle_type_id', 'avg_consumption'];
    
    /**
     * Get the UserStats referring to this location
     */
    public function userstats() {
        return $this->hasMany(UserStat::class);
    }
    
    /**
     * Get the Trips referring to this location
     */
    public function trips() {
        return $this->belongsToMany(Trip::class);
    }
    
    /**
     * Get the vehicle type of this vehicle
     * */
    public function vehicletype(){
        return $this->belongsTo(VehicleType::class);
    }
}
