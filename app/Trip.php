<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

class Trip extends Model
{
    /**
     * Mass assignable attributes.
     * 
     * @var array
     */
    protected $fillable = [ 'start_location_id', 
                            'destination_location_id',
                            'transport_id', 
                            'vehicle_id',
                            'travel_duration',  
                            'travel_distance_km', 
                            'emission_vol'];
    
    /**
     * Get the user associated with the trip.
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /**
     * Get the transportation method associated with the trip.
     */
    public function transportation() {
        return $this->belongsTo(Transportation::class);
    }
    
    /**
     * Get the locations associated with the trip.
     * Should have 2 : start and destination.
     * */
    public function locations(){
        return $this->hasMany(Location::class);
    }
    
    /**
     * Get the vehicle used for this trip.
     * Can be null if mode is not car-related.
     **/
    public function vehicle(){
        return $this->belongsTo(Vehicle::class);
    }
    
     /**
     * See if the current user can view the trip.
     */
    public function userCanView(User $user) {
        return $user->id === $this->user_id;
    }
}
