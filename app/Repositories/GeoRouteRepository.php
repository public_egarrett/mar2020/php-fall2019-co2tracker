<?php
namespace App\Repositories;

use App\Constants;
use UnexpectedValueException;
use InvalidArgumentException;

/**
 * Class to make calls to the HERE API.
 * 
 * Example of usage:
    $repo = new GeoRouteRepository();
    $coordinates = $repo->getGeocode('7000', 'avenue du parc', 'montreal', 'canada');
    $route = $repo->getRouteByCar(52.5,13.4,52.5,13.45, 'gasoline', 5.5);
    $route = $repo->getRouteByCar(52.5,13.4,52.5,13.45, 'electric');
    $route = $repo->getRouteByTransit(52.5,13.4,52.5,13.45);
    $route = $repo->getRouteByBike(52.5,13.4,52.5,13.45);
    $route = $repo->getRouteByWalk(52.5,13.4,52.5,13.45);
    $route = $repo->getRouteBy('car', 52.5,13.4,52.5,13.45, 'gasoline', 5.5);
    $route = $repo->getRouteBy('carpool', 52.5,13.4,52.5,13.45, 'electric');
    $route = $repo->getRouteBy('bike',52.5,13.4,52.5,13.45);
    
    //returns 2 potential addresses
    $result = $repo->getGeocode('200', 'Mathilda', 'Sunnyvale', 'usa'); 
    
    // throws no route found, as cant go by transit to vancouver!
    $result = $repo->getRouteByTransit(52.5,13.4,49.2827,123.1207); 
    
    //throws no route found but could also be waypoint not found
    $result = $repo->getRouteByCar(89,-179,89,-179, 'gasoline', 5.5);
**/
class GeoRouteRepository {

    ////////////////////////////////////////////////////////////////////////////
    //PUBLIC FIELDS
    ////////////////////////////////////////////////////////////////////////////
    //String keys in array returned by getGeocode() function
    public const GEO_CODE_ARRAY_TYPE = 'type';
    public const GEO_CODE_ARRAY_TYPE_ADDRESSES = 'addresses';
    public const GEO_CODE_ARRAY_TYPE_COORDINATES = 'coordinates';
    public const GEO_CODE_ARRAY_LATITUDE = 'latitude';
    public const GEO_CODE_ARRAY_LONGITUDE = 'longitude';
    
    //String keys in array returned by getRouteBy...() functions
    public const ROUTE_ARRAY_DISTANCE = 'distance';
    public const ROUTE_ARRAY_TRAVEL_TIME = 'travelTime';
    
    //Additional string key in array returned by getRouteByCar() function
    public const ROUTE_CAR_ARRAY_EMISSION = 'co2Emission';
    
    //Valid vehicle types
    public const VALID_VEHICLE_TYPES = ['diesel'=>'diesel', 
                                    'gasoline'=>'gasoline', 
                                    'electric'=>'electric'];  
    
    //Exception Messages
    public const ARGUMENT_NOT_SET = 'Argument need to be set!';
    public const ARGUMENT_EMPTY = 'Argument need to be non-empty.';
    public const ARGUMENT_INVALID = 'Argument has an invalid value.'; 
    
    public const GEO_CODE_NOT_FOUND = "GeoCodeNotFound";
    public const NO_ROUTE_FOUND = 'NoRouteFound';
    public const WAYPOINT_NOT_FOUND = 'WaypointNotFound'; //Not caused by invalid argument
    public const INVALID_RESPONSE = 'The API call returned an invalid response.';
    
    ////////////////////////////////////////////////////////////////////////////
    //GEOCODER API FUNCTION
    ////////////////////////////////////////////////////////////////////////////
    /*
     * HERE GeoCoder API 
     * https://developer.here.com/documentation/geocoder/dev_guide/topics/what-is.html
     * 
     * HERE Routing API
     * Car : https://developer.here.com/documentation/routing/dev_guide/topics/request-a-simple-route.html
     * Bike : https://developer.here.com/documentation/routing/dev_guide/topics/bicycle-routing.html
     * Transit : https://developer.here.com/documentation/transit/dev_guide/topics/what-is.html
     * 
     * Example of formed URL:
     * https://route.api.here.com/routing/7.2/calculateroute.json?app_id=recLRHVtpCe791eyKBTj&app_code=8Tq-DM9JRLrQ3sEU9R9s3g&departure=now&routesummarytype=co2emission&mode=fastest;car;traffic:enabled&vehicletype=gasoline,5.5&waypoint0=52.5160,13.3779&waypoint1=52.5206,13.3862
     *
     * APP_ID = 'recLRHVtpCe791eyKBTj'
     * APP_CODE = '8Tq-DM9JRLrQ3sEU9R9s3g'
     */
    ////////////////////////////////////////////////////////////////////////////
    //GEOCODER API FUNCTION
    ////////////////////////////////////////////////////////////////////////////
    //Base URL for geocoder API with API Keys
    private const GEO_CODE_URL
        = 'https://geocoder.api.here.com/6.2/geocode.json?gen=9'
            .'&app_id=recLRHVtpCe791eyKBTj&app_code=8Tq-DM9JRLrQ3sEU9R9s3g';
    
    //Additional URL fields to fill
    private const GEO_CODE_FIELDS = ['streetNumber'=>'housenumber', 
                                        'streetName'=>'street', 
                                        'city'=>'city', 
                                        'country' => 'country'];
    
    /**
     * Get longitude and latitude of the specified address.
     * If this address could correspond to more than one address, get the 
     * different possible addresses.
     * 
     * @return An array.
     * <p>
     * If this address has known coordinates, the value of the string key 
     * GEO_CODE_ARRAY_TYPE is GEO_CODE_ARRAY_TYPE_COORDINATES.
     * It will also hold values for the string keys:
     * GEO_CODE_ARRAY_LATITUDE and GEO_CODE_ARRAY_LONGITUDE
     * 
     * <p>
     * If this address could correspond to more than 1 address, the value of the 
     * string key GEO_CODE_ARRAY_TYPE is GEO_CODE_ARRAY_TYPE_ADDRESSES.
     * It will hold integer keys [0,...] with the different possible addresses.
     * 
     * @throws InvalidArgumentException if arguments not set, empty or invalid.
     * or UnexpectedValueException if waypoint not found or API response is not a JSON.
     */
    public function getGeocode(string $streetNumber, 
                                string $streetName, 
                                string $city, 
                                string $country) : array {
        
        //Arguments validation
        $this->validateArgumentSetAndNonEmpty($streetNumber, 'streetNumber');
        $this->validateArgumentSetAndNonEmpty($streetName, 'streetName');
        $this->validateArgumentSetAndNonEmpty($city, 'city');
        $this->validateArgumentSetAndNonEmpty($country, 'country');
        
        //Make url and call
        $url = $this::GEO_CODE_URL
                .'&'.$this::GEO_CODE_FIELDS['streetNumber'].'='
                .rawurlencode($streetNumber)
                .'&'.$this::GEO_CODE_FIELDS['streetName'].'='
                .rawurlencode($streetName)
                .'&'.$this::GEO_CODE_FIELDS['city'].'='
                .rawurlencode($city)
                .'&'.$this::GEO_CODE_FIELDS['country'].'='
                .rawurlencode($country);
        $result = $this->makeCallAndGetJson($url);

        //Validate JSON result
        $this->checkIfAPIReturnedError($result, $url); //May throw an exception.
        
        if(!isset($result->Response->View[0]->Result[0]->Location
                            ->DisplayPosition->Latitude,
                $result->Response->View[0]->Result[0]->Location
                            ->DisplayPosition->Longitude)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                             . " Needed parameters not found. URL=$url");  
        }

        //It means that more than one address was possible,
        //return an array holding the different possible addresses.
        if(isset($result->Response->View[0]->Result[1])){
            $i=0;
            $addresses=[$this::GEO_CODE_ARRAY_TYPE
                                        =>$this::GEO_CODE_ARRAY_TYPE_ADDRESSES];
            while(isset($result->Response->View[0]->Result[$i])){
                if(isset($result->Response->View[0]
                                        ->Result[$i]->Location->Address->Label)){
                    array_push($addresses, $result->Response->View[0]
                                        ->Result[$i]->Location->Address->Label);
                }
                $i++;
            }
            return $addresses;
        }

        
        //Extract from JSON result
        $latitude = $result->Response->View[0]->Result[0]->Location
                            ->DisplayPosition->Latitude;
        $longitude = $result->Response->View[0]->Result[0]->Location
                            ->DisplayPosition->Longitude;
        
        $coordinates = [$this::GEO_CODE_ARRAY_TYPE =>$this::GEO_CODE_ARRAY_TYPE_COORDINATES,
                        $this::GEO_CODE_ARRAY_LATITUDE=>$latitude, 
                        $this::GEO_CODE_ARRAY_LONGITUDE=>$longitude];

        return $coordinates;                                  
    }


    ////////////////////////////////////////////////////////////////////////////
    //ROUTING API FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////
    //Base URL for routing API with API Keys
    private const ROUTE_URL
    ='https://route.api.here.com/routing/7.2/calculateroute.json?'
    .'app_id=recLRHVtpCe791eyKBTj&app_code=8Tq-DM9JRLrQ3sEU9R9s3g&departure=now';
    
    //Additional URL fields to fill
    private const ROUTE_FIELDS = ['start'=>'waypoint0', //Value='geo!<lat>,<long>'
                            'destination'=>'waypoint1']; //Value='geo!<lat>,<long>'
                            
    //Additional URL for car mode
    private const CAR_URL
        ='&routesummarytype=co2emission&mode=fastest;car;traffic:enabled';
    
    //Additional URL field to fill for car mode
    private const CAR_FIELDS = ['vehicle'=>'vehicletype']; 
                            //Value='<type>,<comsumption>'' 
                            //or ='<type>'' only if electric
        
    //Additional URL for transit mode
    private const TRANSIT_URL
        ='&mode=fastest;publicTransport;traffic:enabled&combineChange=true';
        
    //Additional URL for bike mode
    private const BIKE_URL ='&mode=fastest;bicycle';
    
    //Additional URL for bike mode
    private const WALK_URL ='&mode=fastest;pedestrian';
    
    private const API_ERROR_NO_ROUTE_FOUND = "NoRouteFound";
    private const API_ERROR_INVALID_INPUT = "InvalidInputData";
    private const API_ERROR_WAYPOINT_NOT_FOUND= "WaypointNotFound";
    
    /**
     * Get time and distance of route from start to destination.
     * @param $mode Must be in Constants::TRANSPORTATION_MODES.
     * @param $startLatitude Must be between -90 and 90.
     * @param $startLongitude Must be between -180 and 180.
     * @param $destinationLatitude  Must be between -90 and 90.
     * @param $destinationLongitude Must be between -180 and 180.     
     * @param $vehicleType Must be a value in VALID_VEHICLE_TYPES if mode is 'car'
     * or 'carpool'.
     * @param $averageConsumption In L/100km. Must be a positive number. 
     * Does not need to be specified if mode is not 'car' or 'carpool' or if
     * vehicleType is electric.
     * 
     * @return array ['distance'=> value, 'travelTime'=> value]
     * You may use the constants ROUTE_ARRAY_... constants to get the string keys.
     * or
     * array ['distance'=> value, 'travelTime'=> value, 
     *                  'co2Emission' => value] if the mode is 'car' or 'carpool'
     * You may use the constants ROUTE_ARRAY_... and CAR_ARRAY_... 
     * constants to get the string keys.
     * 
     * @throws InvalidArgumentException if arguments not set, empty or invalid.
     * or UnexpectedValueException if route not found, waypoint not found, 
     * input considered invalid by APIR or API response is not a JSON.
     **/
    public function getRouteBy(string $mode, string $startLatitude, 
                                    string $startLongitude,
                                    string $destinationLatitude, 
                                    string $destinationLongitude,
                                    string $vehicleType=null,
                                    float $averageConsumption=0.0) : array {
        
        //Arguments validation
        $this->validateArgumentSetAndNonEmpty($mode, 'mode');
        $this->validateArgumentSetAndNonEmpty($startLatitude, 'startLatitude');
        $this->validateArgumentSetAndNonEmpty($startLongitude, 'startLongitude');
        $this->validateArgumentSetAndNonEmpty($destinationLatitude, 'destinationLatitude');
        $this->validateArgumentSetAndNonEmpty($destinationLongitude, 'destinationLongitude');
        
        $mode=strtolower($mode);
        
        if(!in_array($mode, Constants::TRANSPORTATION_MODES_LOWERCASE)){
            throw new InvalidArgumentException($this::ARGUMENT_INVALID
                .' Argument given for mode : '. $mode . ' is invalid.');
        }
        
        if(strcmp($mode, 'transit')==0){
            return $this->getRouteByTransit($startLatitude, $startLongitude, 
            $destinationLatitude, $destinationLongitude);
        } 
        
        else if(strcmp($mode, 'bike')==0){
            return $this->getRouteByBike($startLatitude, $startLongitude, 
            $destinationLatitude, $destinationLongitude);
        } 
        
        else if(strcmp($mode, 'walk')==0){
            return $this->getRouteByWalk($startLatitude, $startLongitude, 
            $destinationLatitude, $destinationLongitude);
        } 
        
        else if(strcmp($mode, 'car')==0 || strcmp($mode, 'carpool')==0){
            return $this->getRouteByCar($startLatitude, $startLongitude, 
            $destinationLatitude, $destinationLongitude, $vehicleType, $averageConsumption);
        } 
        
        else {
            throw new InvalidArgumentException("Argument mode invalid.
             Given value : ".$mode);
        }
    }

    /**
     * Get route from start to destination, using the car mode.
     * @param $startLatitude Must be between -90 and 90.
     * @param $startLongitude Must be between -180 and 180.
     * @param $destinationLatitude  Must be between -90 and 90.
     * @param $destinationLongitude Must be between -180 and 180.
     * @param $vehicleType Must be a value in VALID_VEHICLE_TYPES
     * @param $averageConsumption In L/100km. Must be a positive number. 
     * Does not need to be specified if vehicleType is electric.
     * 
     * @return An array ['distance'=> value, 'travelTime'=> value, 
     *                  'co2Emission' => value]
     * You may use the constants ROUTE_ARRAY_... and CAR_ARRAY_... 
     * constants to get the string keys.
     * Distances are in meters, travelTime in seconds, and co2Emissions in kg.
     * 
     * @throws InvalidArgumentException if arguments not set, empty or invalid.
     * or UnexpectedValueException if route not found, waypoint not found, 
     * input considered invalid by APIR or API response is not a JSON.
     **/
    public function getRouteByCar(string $startLatitude, 
                                    string $startLongitude,
                                    string $destinationLatitude, 
                                    string $destinationLongitude,
                                    string $vehicleType, 
                                    float $averageConsumption=0.0) : array {
        //Arguments validation
        $this->validateArgumentSetAndNonEmpty($startLatitude, 'startLatitude');
        $this->validateArgumentSetAndNonEmpty($startLongitude, 'startLongitude');
        $this->validateArgumentSetAndNonEmpty($destinationLatitude, 'destinationLatitude');
        $this->validateArgumentSetAndNonEmpty($destinationLongitude, 'destinationLongitude');

        
        if(!in_array($vehicleType, $this::VALID_VEHICLE_TYPES)){
            throw new InvalidArgumentException($this::ARGUMENT_INVALID.
                    ' Invalid vehicleType argument given : '.$vehicleType);
        }
        
        if(round($averageConsumption, 2) < 0.0){
            throw new InvalidArgumentException($this::ARGUMENT_INVALID.
            ' Invalid averageConsumption argument given : '.$averageConsumption
            .' Must be higher than 0.');
        }
        $this->validateLatitudeValue($startLatitude, 'startLatitude');
        $this->validateLongitudeValue($startLongitude, 'startLongitude');
        $this->validateLatitudeValue($destinationLatitude, 'destinationLatitude');
        $this->validateLongitudeValue($destinationLongitude, 'destinationLongitude');
        

        //Make url and call
        if(strcmp($vehicleType, $this::VALID_VEHICLE_TYPES['electric']) == 0){
            $url = $this::ROUTE_URL.$this::CAR_URL
            .'&'.$this::ROUTE_FIELDS['start'].'=geo!'.$startLatitude
            .','.$startLongitude
            .'&'.$this::ROUTE_FIELDS['destination'].'=geo!'.$destinationLatitude
            .','.$destinationLongitude
            .'&'.$this::CAR_FIELDS['vehicle'].'='.rawurlencode($vehicleType);
        } else {
            $url = $this::ROUTE_URL.$this::CAR_URL
            .'&'.$this::ROUTE_FIELDS['start'].'=geo!'.$startLatitude
            .','.$startLongitude
            .'&'.$this::ROUTE_FIELDS['destination'].'=geo!'.$destinationLatitude
            .','.$destinationLongitude
            .'&'.$this::CAR_FIELDS['vehicle'].'='.rawurlencode($vehicleType)
            .','.$averageConsumption;
        }     
        
        $result = $this->makeCallAndGetJson($url);
        
        //Validate JSON result
        $this->checkIfAPIReturnedError($result, $url); //May throw an exception.
        
        //Extract infos from JSON (returns null if not valid)
        $routeInfos = $this->validateAndExtractInfosFromCarRouteJson($result);
        
        //Validate
        if(is_null($routeInfos)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                            ." URL='$url'");
        }
       
        //Return
        return $routeInfos;
    }

    
    

    /**
     * Get time and distance of route from start to destination, by transit.
     * @param $startLatitude Must be between -90 and 90.
     * @param $startLongitude Must be between -180 and 180.
     * @param $destinationLatitude  Must be between -90 and 90.
     * @param $destinationLongitude Must be between -180 and 180.
     * @return array ['distance'=> value, 'travelTime'=> value]
     * You may use the constants ROUTE_ARRAY_... constants to get the string keys.
     * Distances are in meters, travelTime in seconds.
     * 
     * @throws InvalidArgumentException if arguments not set, empty or invalid.
     * or UnexpectedValueException if route not found, waypoint not found, 
     * input considered invalid by APIR or API response is not a JSON.
     **/
    public function getRouteByTransit(string $startLatitude, 
                                    string $startLongitude,
                                    string $destinationLatitude, 
                                    string $destinationLongitude) : array {
        
        //Arguments validation
        $this->validateArgumentSetAndNonEmpty($startLatitude, 'startLatitude');
        $this->validateArgumentSetAndNonEmpty($startLongitude, 'startLongitude');
        $this->validateArgumentSetAndNonEmpty($destinationLatitude, 'destinationLatitude');
        $this->validateArgumentSetAndNonEmpty($destinationLongitude, 'destinationLongitude');
        $this->validateLatitudeValue($startLatitude, 'startLatitude');
        $this->validateLongitudeValue($startLongitude, 'startLongitude');
        $this->validateLatitudeValue($destinationLatitude, 'destinationLatitude');
        $this->validateLongitudeValue($destinationLongitude, 'destinationLongitude');
        
        //Make url and call
       $url = $this::ROUTE_URL.$this::TRANSIT_URL
            .'&'.$this::ROUTE_FIELDS['start'].'=geo!'.$startLatitude
            .','.$startLongitude
            .'&'.$this::ROUTE_FIELDS['destination'].'=geo!'.$destinationLatitude
            .','.$destinationLongitude;
                                                    
        $result = $this->makeCallAndGetJson($url);
        
        //Validate JSON result
        $this->checkIfAPIReturnedError($result, $url); //May throw an exception.
        
        //Extract infos from JSON (returns null if not valid)
        $routeInfos = $this->validateAndExtractInfosFromRouteJson($result);
        
        //Validate
        if(is_null($routeInfos)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                            ." URL='$url'");
        }
       
        //Return
        return $routeInfos;
    }
    
    /**
     * Get time and distance of route from start to destination, by bike.
     * @param $startLatitude Must be between -90 and 90.
     * @param $startLongitude Must be between -180 and 180.
     * @param $destinationLatitude  Must be between -90 and 90.
     * @param $destinationLongitude Must be between -180 and 180.
     * @return array ['distance'=> value, 'travelTime'=> value]
     * You may use the constants ROUTE_ARRAY_... constants to get the string keys.
     * Distances are in meters, travelTime in seconds.
     * 
     * @throws InvalidArgumentException if arguments not set, empty or invalid.
     * or UnexpectedValueException if route not found, waypoint not found, 
     * input considered invalid by APIR or API response is not a JSON.
     **/
    public function getRouteByBike(string $startLatitude, string $startLongitude,
                                    string $destinationLatitude, 
                                    string $destinationLongitude) : array {

        //Arguments validation
        $this->validateArgumentSetAndNonEmpty($startLatitude, 'startLatitude');
        $this->validateArgumentSetAndNonEmpty($startLongitude, 'startLongitude');
        $this->validateArgumentSetAndNonEmpty($destinationLatitude, 'destinationLatitude');
        $this->validateArgumentSetAndNonEmpty($destinationLongitude, 'destinationLongitude');
        $this->validateLatitudeValue($startLatitude, 'startLatitude');
        $this->validateLongitudeValue($startLongitude, 'startLongitude');
        $this->validateLatitudeValue($destinationLatitude, 'destinationLatitude');
        $this->validateLongitudeValue($destinationLongitude, 'destinationLongitude');
        
        //Make url and call
       $url = $this::ROUTE_URL.$this::BIKE_URL
            .'&'.$this::ROUTE_FIELDS['start'].'=geo!'.$startLatitude
            .','.$startLongitude
            .'&'.$this::ROUTE_FIELDS['destination'].'=geo!'.$destinationLatitude
            .','.$destinationLongitude;
                                                    
        $result = $this->makeCallAndGetJson($url);
        
        //Validate JSON result
        $this->checkIfAPIReturnedError($result, $url); //May throw an exception.
        
        //Extract infos from JSON (returns null if not valid)
        $routeInfos = $this->validateAndExtractInfosFromRouteJson($result);
        
        //Validate
        if(is_null($routeInfos)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                            ." URL='$url'");
        }
       
        //Return
        return $routeInfos;
    }
    
    /**
     * Get time and distance of route from start to destination, walking.
     * @param $startLatitude Must be between -90 and 90.
     * @param $startLongitude Must be between -180 and 180.
     * @param $destinationLatitude  Must be between -90 and 90.
     * @param $destinationLongitude Must be between -180 and 180.
     * @return array ['distance'=> value, 'travelTime'=> value]
     * You may use the constants ROUTE_ARRAY_... constants to get the string keys.
     * Distances are in meters, travelTime in seconds.
     * 
     * @throws InvalidArgumentException if arguments not set, empty or invalid.
     * or UnexpectedValueException if route not found, waypoint not found, 
     * input considered invalid by APIR or API response is not a JSON.
     **/
    public function getRouteByWalk(string $startLatitude, string $startLongitude,
                                    string $destinationLatitude, 
                                    string $destinationLongitude) : array {

        //Arguments validation
        $this->validateArgumentSetAndNonEmpty($startLatitude, 'startLatitude');
        $this->validateArgumentSetAndNonEmpty($startLongitude, 'startLongitude');
        $this->validateArgumentSetAndNonEmpty($destinationLatitude, 'destinationLatitude');
        $this->validateArgumentSetAndNonEmpty($destinationLongitude, 'destinationLongitude');
        $this->validateLatitudeValue($startLatitude, 'startLatitude');
        $this->validateLongitudeValue($startLongitude, 'startLongitude');
        $this->validateLatitudeValue($destinationLatitude, 'destinationLatitude');
        $this->validateLongitudeValue($destinationLongitude, 'destinationLongitude');
        
        //Make url and call
       $url = $this::ROUTE_URL.$this::WALK_URL
            .'&'.$this::ROUTE_FIELDS['start'].'=geo!'.$startLatitude
            .','.$startLongitude
            .'&'.$this::ROUTE_FIELDS['destination'].'=geo!'.$destinationLatitude
            .','.$destinationLongitude;
                                                    
                                                    
        $result = $this->makeCallAndGetJson($url);
        
        //Validate JSON result
        $this->checkIfAPIReturnedError($result, $url); //May throw an exception.
        
        //Extract infos from JSON (returns null if not valid)
        $routeInfos = $this->validateAndExtractInfosFromRouteJson($result);
        
        //Validate
        if(is_null($routeInfos)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                            ." URL='$url'");
        }
       
        //Return
        return $routeInfos;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //HELPER FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////
    
    //Validate argument is set and non-empty
    private function validateArgumentSetAndNonEmpty($argument, $argumentName){
        if(!isset($argument)){
            throw new InvalidArgumentException($this::ARGUMENT_NOT_SET. 
            'Argument: name= '.$argumentName.' given value='.$argument);
        }
        
        if(is_string($argument) && strlen($argument) == 0){
            throw new InvalidArgumentException($this::ARGUMENT_EMPTY. 
            'Argument: name= '.$argumentName.' given value='.$argument);
        }
    }
    
    //Validate longitude is within range
    private function validateLongitudeValue($longitude, $argumentName){
        if (round($longitude, 2) <= Constants::LONGITUDE_MIN_VALUE 
                || round($longitude, 2) >= Constants::LONGITUDE_MAX_VALUE){
                    
            throw new InvalidArgumentException($this::ARGUMENT_INVALID.
            $argumentName.' must be in range ]'. Constants::LONGITUDE_MIN_VALUE
            . ', '.Constants::LONGITUDE_MAX_VALUE.'[. Argument given : '.$longitude);
            
        }
    }
    
    //Validate latitude is within range
    private function validateLatitudeValue($latitude, $argumentName){
        if (round($latitude, 2) <= Constants::LATITUDE_MIN_VALUE 
                || round($latitude, 2) >= Constants::LATITUDE_MAX_VALUE){
                    
            throw new InvalidArgumentException($this::ARGUMENT_INVALID.
            $argumentName.' must be in range ]'. Constants::LATITUDE_MIN_VALUE
            . ', '.Constants::LATITUDE_MAX_VALUE.'[. Argument given : '.$latitude);
            
        }
    }
    
    //Make call with the specified url
    private function makeCallAndGetJson($url){
        $curl=curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($curl));
        curl_close($curl);
        return $result;
    }
    
    //If API returned an error, throw exception
    private function checkIfAPIReturnedError($result, $url) {
        
        //If $result not even set or a JSON object
        if(!isset($result) || !is_object($result)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                            ." Response is not set or not a JSON. URL='$url'");
        }
        
        //Depending of error type, throw adequate exception
        if(isset($result->subtype, $result->details)){
            switch($result->subtype){
                case 'NoRouteFound':
                    throw new UnexpectedValueException($this::NO_ROUTE_FOUND);
                    break;
                case 'InvalidInputData':
                    throw new InvalidArgumentException($this::ARGUMENT_INVALID
                    .' Error type: '. $result->details);
                    break;
                default:
                    throw new UnexpectedValueException($this::INVALID_RESPONSE
                    .' Error type: '. $result->subtype .' : '.$result->details
                    ." URL='$url'" );
                }
        
        } 
        
        //If only 'subtype' is specified in error response
        else if(isset($result->subtype)){
            switch($result->subtype){
                case 'NoRouteFound':
                    throw new UnexpectedValueException($this::NO_ROUTE_FOUND);
                    break;
                case 'InvalidInputData':
                    throw new InvalidArgumentException($this::ARGUMENT_INVALID);
                    break;
                default:
                    throw new UnexpectedValueException($this::INVALID_RESPONSE
                    .' Error type: '. $result->subtype ." URL='$url'" );
            }
        } else 
        
        //If only 'type' is specified in the error response
        if (isset($result->type)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                            .' Error type: '. $result->type ." URL='$url'" );
        }
    }

    //Extract car infos from route JSON
    //Returns null if needed fields not found
    private function validateAndExtractInfosFromCarRouteJson($result) : ?array{
        $routeInfos = $this->validateAndExtractInfosFromRouteJson($result);
        
        //If invalid
        if(!isset($result) || !is_object($result) || !isset($routeInfos)){
            throw new UnexpectedValueException($this::INVALID_RESPONSE
                            ." URL='$url'");
        }

        //If valid, add emission
        $emission = $result->response->route[0]->summary->co2Emission;
        $routeInfos[$this::ROUTE_CAR_ARRAY_EMISSION]=$emission;
        return $routeInfos;
    }
    
    //Extract route infos from route JSON, use for transit, bike, walk
    //Returns null if needed fields not found
    private function validateAndExtractInfosFromRouteJson($result) : ?array{
        //If invalid
        if(!isset($result) || !is_object($result)
            || !isset($result->response->route[0]->summary->distance, 
                    $result->response->route[0]->summary->travelTime)){
            return null;           
        }

        //If valid
        $distance = $result->response->route[0]->summary->distance;
        $traveltime = $result->response->route[0]->summary->travelTime;
        $infos = [$this::ROUTE_ARRAY_DISTANCE => $distance, 
                    $this::ROUTE_ARRAY_TRAVEL_TIME => $traveltime];
        return $infos;
    }
}