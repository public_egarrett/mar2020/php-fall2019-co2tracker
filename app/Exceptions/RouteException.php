<?php
use Exception;
namespace App\Exceptions;

/**
 * 
 * An Exception indicating a problem with a Route not found, or other Route problems.
 * */
class RouteException extends Exception { 
    
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->view('common.invalidroute', ['errors' => $this], 500);
    }
    
}