<?php
use Exception;
namespace App\Exceptions;

/**
 * 
 * An Exception indicating when a call could correspond to more than one address.
 * */
class TooManyLocationsException extends Exception { 
    
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->view('common.manyaddresses', ['errors' => $this], 500);
    }
    
}