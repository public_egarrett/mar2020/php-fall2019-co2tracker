<?php

namespace App\Exceptions;
use Exception;

/**
 * 
 * An Exception indicating a location could not be found.
 * */
class NoLocationFoundException extends Exception { 
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
  /*  public function render($request)
    {
        return response()->view('common.noaddresses', ['errors' => $this], 500);
    }*/
    
}