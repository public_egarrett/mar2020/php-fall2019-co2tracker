<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

class Location extends Model
{
    protected $fillable = ['latitude', 'longitude'];
    
    /**
     * Get the UserStats referring to this location
     */
    public function userstats() {
        return $this->hasMany(UserStat::class);
    }
    
    /**
     * Get the Trips referring to this location
     */
    public function trips() {
        return $this->belongsToMany(Trip::class);
    }
}
