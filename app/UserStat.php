<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

class UserStat extends Model
{
    /**
     * Define attributes that are mass-assignable.
     * 
     * @var array
     */
    protected $fillable = ['user_id', 
                            'home_location_id',
                            'vehicle_id', 
                            'total_commute_km', 
                            'yearly_emissions', 
                            'offset'];
    
    /**
     * Get the user belong to the stats.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Get the vehicle of this user
     **/
    public function vehicle(){
        return $this->belongsTo(Vehicle::class);
    }

    /**
     * Get location of this user
     * */
    public function homelocation(){
        return $this->belongsTo(Location::class, 'home_location_id');
    }
}
