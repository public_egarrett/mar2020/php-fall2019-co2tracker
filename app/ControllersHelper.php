<?php

namespace App;
use DateTime;

/**
 * Class with static methods to combine common needs in
 * the (wep api) ApiController and the (web page) RouteSelectController.
 **/
class ControllersHelper {
    
    /**
     * Calculates the total CO2 emissions of the 
     * specified user for the given year.
     * @return total co2 emissions in kg for the year
     */
    public static function calculateYearlyEmissions($user) : float {

        $trips = Trip::where('user_id', $user->id)->get();
        
        $total = 0;
        
        $today = new DateTime();
        $year = $today->format('Y');
        
        foreach ($trips as $trip) {
            $time = $trip->created_at->format('Y');
            
            if ($year == $time) {
                $total += $trip->emission_vol;
            }
        }
        
        return $total;
    }
    
   /**
     * Calculate's a user's total lifetime emissions.
     * @return total lifetime co2 emissions in kg.
     */
    public static function calculateTotalEmissions($user) : float {
        
        $trips = Trip::where('user_id', $user->id)->get();
        
        $total = 0;
        
        foreach ($trips as $trip) {
            $total += $trip->emission_vol;
        }
        
        return $total;
    }
    
    /**
     * Calculates the offset based on the emissions in kg.
     * 
     * @param $emissions in kg
     * @return The carbon offset cost, estimated at $30 / tonne.
     */
    public static function calculateOffset($emissions) : float {
        return $emissions * Constants::OFFSET_PRICE_PER_KG;
    }
    
    /**
     * Updates the user_stats with this new trip distance and emissions.
     * @param distance in km
     * @param emissions in kg
     * */
    public static function updateUserStats($user, $distance, $emissions){
        $stats=$user->userstat;
        //Update total distance
        $currentDistanceTotal = $stats->total_commute_km;
        $updatedDistance = $currentDistanceTotal + $distance;
        
        //Get this year's emissions and offset
        $currentYearEmissions = self::calculateYearlyEmissions($user);
        $currentYearOffset = $currentYearEmissions * Constants::OFFSET_PRICE_PER_KG;
        
        //Update db
        $stats->total_commute_km = $updatedDistance;
        $stats->yearly_emissions = $currentYearEmissions;
        $stats->offset = $currentYearOffset;
        $stats->save();
        

        /*
        echo '----------------------------'."\n";
        echo 'trip distance : '. $distance."\n";
        echo 'trip emissions : ' .$emissions."\n";
        echo '----------------------------'."\n";*/
        
        
        /*echo 'total distance : '. $updatedDistance."\n";
        echo 'year emissions : ' .$currentYearEmissions."\n";
        echo 'year offset : '. $currentYearOffset."\n";
        echo '----------------------------'."\n";*/
    }
}