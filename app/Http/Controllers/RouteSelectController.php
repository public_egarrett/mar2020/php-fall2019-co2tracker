<?php
namespace App\Http\Controllers;

use App\Repositories\GeoRouteRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use ControllersHelper;

use App\User;
use App\UserStat;
use App\VehicleType;
use App\Trip;
use App\Location;
use App\Transportation;
use App\Exceptions\NoLocationFoundException;
use App\Exceptions\TooManyLocationsException;
use App\Exceptions\RouteException;
use App\Constants;

use DateTime;

class RouteSelectController extends Controller {
    private $geoRouteRepo;
    
    //Constants for frequently-used values
    private const LATITUDE = 'latitude';
    private const LONGITUDE = 'longitude';
    private const HOME = "home";
    private const DAWSON = "dawson";
    private const NEW = "new";
    
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    public function __construct() {
        $this->geoRouteRepo = new GeoRouteRepository();
        $this->middleware('auth');
    }
    
    /**
     * Show view.
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('/home.routeselect');
    }
    
    /**
     * Retrieves, stores, and displays a list of route options from geoRouteRepository and uses to generate table.
     * 
     * @param $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function findroutes(Request $request) {
        $start = $this->getStartAddress($request);
        $destination = $this->getDestinationAddress($request);
        
        if(!array_key_exists($this::LATITUDE, $start) ||
            !array_key_exists($this::LONGITUDE, $start) ||
                !array_key_exists($this::LATITUDE, $destination) ||
                    !array_key_exists($this::LONGITUDE, $destination) ){
            throw new NoLocationFoundException("Addresses couldn't be found by API.");
        }
        
        $stats = Auth::user()->userstat;
        
        $startLocation = Location::firstOrCreate([
            $this::LATITUDE => $start[$this::LATITUDE],
            $this::LONGITUDE => $start[$this::LONGITUDE]
        ]);
        
        //Get or create the end location
        $endLocation = Location::firstOrCreate([
            $this::LATITUDE => $destination[$this::LATITUDE],
            $this::LONGITUDE => $destination[$this::LONGITUDE]
        ]);

        //Retrieve mode of transport associated with the user
        $method = null;
        if ($stats->vehicle_id != null) {
            $userVehicle = $stats->vehicle;
            $method = VehicleType::where('id', $userVehicle->vehicle_type_id)->first();
        } 
        
        $routes = array();
        
        //Get all possible routes, based on authenticated user's selected vehicle type.
        if ($method != null) {
            $routes['Car'] = $this->getRoute($startLocation, $endLocation, 'Car');
            $routes['Carpool'] = $this->getRoute($startLocation, $endLocation, 'Carpool');
        }
        
        $routes['Transit'] = $this->getRoute($startLocation, $endLocation, 'Transit');
        $routes['Bike'] = $this->getRoute($startLocation, $endLocation, 'Bike');
        $routes['Walk'] = $this->getRoute($startLocation, $endLocation, 'Walk');
        
        $this->storedRoutes = $routes;
        
        return view('home.routeselect', [
            'routes' => $routes,
            'start' => $startLocation->id,
            'end' => $endLocation->id
        ]);
    }
    
    /**
     * Gets route information based on route type selected.
     * 
     */
    private function getRoute($start, $end, $type) {
        if (Auth::user()->userstat->vehicle_id != null) {
            $userVehicle = Auth::user()->userstat->vehicle;
            $method = VehicleType::where('id', $userVehicle->vehicle_type_id)->first();
        }
        try {
            switch($type) {
                case 'Car':
                    return $this->geoRouteRepo->getRouteByCar($start->latitude, $start->longitude, $end->latitude, $end->longitude, $method->type, $userVehicle->avg_consumption);
                    break;
                case 'Carpool':
                    return $this->geoRouteRepo->getRouteByCar($start->latitude, $start->longitude, $end->latitude, $end->longitude, $method->type, $userVehicle->avg_consumption);
                    break;
                case 'Transit':
                    return $this->geoRouteRepo->getRouteByTransit($start->latitude, $start->longitude, $end->latitude, $end->longitude);
                    break;
                case 'Bike':
                    return $this->geoRouteRepo->getRouteByBike($start->latitude, $start->longitude, $end->latitude, $end->longitude);
                    break;
                case 'Walk':
                    return $this->geoRouteRepo->getRouteByWalk($start->latitude, $start->longitude, $end->latitude, $end->longitude);
                    break;
            }
        } catch (\Exception $e) {
            throw new RouteException("Route by $type could not be computed.");
        }
    }
    
    
    /**
     * Stores a new trip and redirects user to their home page.
     * 
     * @param $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request) {
        $selected = $request->input('method');
        
        //Get the start location
        $startLocation = Location::where('id', $request->input('start_location'))->first();
        
        //Get the end location
        $endLocation = Location::where('id', $request->input('end_location'))->first();
        
        //Get the transportation method id
        $transport = Transportation::where('transportation_method', strtolower($selected))->first();
        
        //Get the selected route
        $route = $this->getRoute($startLocation, $endLocation, $selected);
        
        
        //Set the emission volume based on type
        $emissions = 0;
        $numpassengers = 2;
        
        if ($selected == 'Car') {
            $emissions = $route['co2Emission'];
        } else if ($selected == 'Carpool') {
                $numpassengers = (int) $request->input('numpassengers');
                $emissVal = (double) $route['co2Emission'];
                $emissions = $emissVal / $numpassengers;
        } else if ($selected == 'Transit') {
            //Distance / 1000 = distance in km --> distance in km * 4.62 = g/km --> g/km / 1000 = kg/km
            $emissions = $route['distance'] / 1000 * 4.62 / 1000;
        }
        
        $vehicle = null;
        
        if ($selected == 'Car' || $selected == 'Carpool') {
            $vehicle = Auth::user()->userstat->vehicle_id;
        }
        
        //Create a new trip
        $request->user()->trips()->create([
            'start_location_id' => $startLocation->id,
            'destination_location_id' => $endLocation->id,
            'travel_duration' => $route['travelTime'],
            'travel_distance_km' => $route['distance'] / 1000,
            'emission_vol' => $emissions,
            'transport_id' => $transport->id,
            'vehicle_id' => $vehicle,
        ]);
        
        //Update fields in userstats
        ControllersHelper::updateUserStats(Auth::user(), $route['distance'] / 1000, $emissions );
        
        return redirect('home\index');
    }

    
    /**
     * Generates starting coordinates based on request input.
     * 
     * @param $request
     * @return an array containing a longitude and latitude value
     */
    private function getStartAddress(Request $request) : array {
        $startaddress = [];
        
        //Check if a new start address has been entered
        if ($request->input('start-address') == $this::NEW && $request->input('new-start-streetnum', null) != null) {
            //Generate start address
            $start = [
                'streetnum' => rawurlencode(htmlentities($request->input('new-start-streetnum'))),
                'street' => rawurlencode(htmlentities($request->input('new-start-street'))),
                'city' => rawurlencode(htmlentities($request->input('new-start-city'))),
                'country' => rawurlencode(htmlentities($request->input('new-start-country')))
            ];
            
            try {
                $coords = $this->geoRouteRepo->getGeocode($start['streetnum'], $start['street'], $start['city'], $start['country']);
            } catch (\Exception $e){
                 throw new NoLocationFoundException("Start address getGeocode call was unsuccessful"); 
            }
            
            
            //Validate result
            if(!is_array($coords)){
                throw new NoLocationFoundException("Start address getGeocode call didnt return an array");
            }
            //this means more than 1 address could correspond
            else if(array_key_exists($this->geoRouteRepo::GEO_CODE_ARRAY_TYPE_ADDRESSES, $coords)){
                throw new TooManyLocationsException("Start address input could correspond to more than 1 address");
            } 
            
            else if(!array_key_exists('longitude', $coords)
                        || !array_key_exists('latitude', $coords)){
                throw new NoLocationFoundException("Start address getGeocode call was unsuccessful"); 
            }
            
            $startaddress = [
                $this::LATITUDE => $coords[$this::LATITUDE],
                $this::LONGITUDE => $coords[$this::LONGITUDE]
            ];
            
        } elseif($request->input('start-address') == $this::HOME) {
            $userstats = $request->user()->userstat;
            
            $startaddress = [
                $this::LATITUDE => $userstats->homelocation->latitude,
                $this::LONGITUDE => $userstats->homelocation->longitude
            ];
        } elseif($request->input('start-address') == $this::DAWSON) {
            $startaddress = [
                $this::LATITUDE => Constants::DAWSON_LATITUDE,
                $this::LONGITUDE => Constants::DAWSON_LONGITUDE
            ];
        }
        
        return $startaddress;
    }
    
    /**
     * Extracts the destination address from the POST request.
     * 
     * @param $request
     * @return an array containing the latitude and longitude of the destination address.
     */
    private function getDestinationAddress(Request $request) : array {
        $destinationaddress = [];
        
        //Check if a new start address has been entered
        if ($request->input('destination-address') == $this::NEW 
            && $request->input('new-destination-streetnum', null) != null) {
      
            //Generate start address
            $end = [
                'streetnum' => $request->input('new-destination-streetnum'),
                'street' => $request->input('new-destination-street'),
                'city' => $request->input('new-destination-city'),
                'country' => $request->input('new-destination-country')
            ];
            
            try {
                $coords = $this->geoRouteRepo->getGeocode($end['streetnum'], $end['street'], $end['city'], $end['country']);
            } catch (\Exception $e){
                 throw new NoLocationFoundException("Destination address getGeocode call was unsuccessful"); 
            }
            
            
            //Validate result
            if(!is_array($coords)){
                throw new NoLocationFoundException("Destination address getGeocode call didnt return an array");
            }
            //this means more than 1 address could correspond
            else if( array_key_exists($this->geoRouteRepo::GEO_CODE_ARRAY_TYPE_ADDRESSES, $coords)){
                throw new TooManyLocationsException("Destination address input could correspond to more than 1 address");
            } else if(!array_key_exists('longitude', $coords)
                        || !array_key_exists('latitude', $coords)){
                throw new NoLocationFoundException("Destination address getGeocode call was unsuccessful"); 
            }
            
            $destinationaddress = [
                $this::LATITUDE => $coords[$this::LATITUDE],
                $this::LONGITUDE => $coords[$this::LONGITUDE]
            ];
            
        } elseif($request->input('destination-address') == $this::HOME) {
            $userstats = $request->user()->userstat;
            
            $destinationaddress = [
                $this::LATITUDE => $userstats->homelocation->latitude,
                $this::LONGITUDE => $userstats->homelocation->longitude
            ];
        } elseif($request->input('destination-address') == $this::DAWSON) {
            $destinationaddress = [
                $this::LATITUDE => Constants::DAWSON_LATITUDE,
                $this::LONGITUDE => Constants::DAWSON_LONGITUDE
            ];
        }
        
        return $destinationaddress;
    }
}
?>