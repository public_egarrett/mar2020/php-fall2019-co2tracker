<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Trip;
use App\Transportation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the web homepage if not authenticated.
     * If authenticated, show the user's stats and trips.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check()){
            //Get all trips
            $trips = Trip::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(5);
            
            //Retrieve all transport methods
            $transportations = Transportation::orderBy('id', 'DESC')->get();
            
            $transportlist = array();
            
            //Use values to populate array
            foreach($transportations as $transport) {
                $transportlist[$transport->id] = $transport->transportation_method;
            }
            
            $userHome = Auth::user()->userstat->homelocation;
            
            return view('/home.index', [ 'trips' => $trips, 'transportations' => $transportlist, 'home' => $userHome ]);
        } else {
            return view('/home.index');
        }
    }
}
