<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;

use ControllersHelper;

use App\Trip;
use App\UserStat;
use App\Vehicle;
use App\VehicleType;
use App\Constants;
use App\Repositories\GeoRouteRepository;
use App\Location;
use App\Transportation;

class ApiController extends Controller {
    
    private $routeRepository;
    
    const INVALID_TOKEN = 'invalid_token';
    const INVALID_INPUT = 'invalid_parameter';
    const APPLICATION_ERROR = 'application_error';
    
    
    public function __construct(){
        $this->routeRepository = new GeoRouteRepository();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //GET ALL TRIPS
    ////////////////////////////////////////////////////////////////////////////
    /**
     *  Returns JSON with all trips of the user with authenticated JWT.
     *  Returns 401 UNAUTHORIZED if token not valid.
     **/
    public function getAllTrips(){
        $user = auth('api')->user();
        
        //Authorize
        if(!$user) {
            return $this->returnErrorResponse(401, $this::INVALID_TOKEN, $this::INVALID_TOKEN);
        } 
        
        //Fetch user's trips
        $trips = Trip::where('user_id', '=', $user->id)->get();
        
        //Format infos for return
        $results=[]; 
        foreach($trips as $trip){
            array_push($results, $this->buildTripArrayFromDatabase($trip));
        }
        return response()->json($results, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8']);
    } 
    
    ////////////////////////////////////////////////////////////////////////////
    //GET TRIP INFO
    ////////////////////////////////////////////////////////////////////////////
    /**
     * May return:
     *  - HTTP Status 200 and JSON with information about potential trip for
     *  the user with authenticated JWT.
     *  - HTTP Status 401 with $this::INVALID_TOKEN message 
     *      if authentication token not valid.
     *  - HTTP Status 422 with $this::INVALID_INPUT message 
     *      and list of errors if input data was invalid.
     *  - HTTP Status 500 with $this::APPLICATION_ERROR message  
     *      and additional detailed message if route could not get
     *      returned or something else unexpected occured. (May be because 
     *      no route is possible, or a location can not be found.)
     **/
    public function getTripInfo(Request $request){
        $user = auth('api')->user();
        
        //Do authorization
        if(!$user) {
            return response()->json(['error' => $this::INVALID_TOKEN], 401);
        } 
        
        //Get all URL GET parameters and validate them
        $input=$this->formatInput($request->all());
        
        $validator = $this->getValidatorForRouteParameters($input);
        
        if($validator->fails()){
            return $this->returnErrorResponse(401, $this::INVALID_TOKEN, $this::INVALID_TOKEN);
        }
        
        //Try getting routing info, can create error responses
        $returnValue = $this->tryAndGetRoutingInfos($input, $user);
        
        //Check if it returned an array with information or array with error
        if(!is_array($returnValue) || isset($returnValue['error'])){
            //It is an error response
            return $returnValue;
        }

        //Otherwise, it is an array with information
        $routeInfoFromRepo = $returnValue['routingInfos'];
        
        $result=[];
        $result['distance'] 
            = $routeInfoFromRepo[$this->routeRepository::ROUTE_ARRAY_DISTANCE];
            
        $result['traveltime'] 
            = $routeInfoFromRepo[$this->routeRepository::ROUTE_ARRAY_TRAVEL_TIME];
        
        if($input['mode']=='transit'){
            $result['co2emissions'] = $result['distance'] / 1000.0
                            * Constants::TRANSIT_EMISSIONS_KG_PER_KM;
        } 
        
        else if ($input['mode']=='car'){
           $result['co2emissions'] 
            = $routeInfoFromRepo[$this->routeRepository::ROUTE_CAR_ARRAY_EMISSION];                 
        } 
        
        else if ($input['mode']=='carpool'){
            $result['co2emissions'] 
            = $routeInfoFromRepo[$this->routeRepository::ROUTE_CAR_ARRAY_EMISSION]
                / Constants::CARPOOL_EMISSION_DIVISION_FACTOR;   
        } 
        
        else {
            $result['co2emissions'] = 0;
        }
        //If here, means no error
        return response()->json($result, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8']);
    }

    
    ////////////////////////////////////////////////////////////////////////////
    //ADD TRIP
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * May return:
     *  - HTTP Status 200 and JSON with information about potential trip for
     *  the user with authenticated JWT.
     *  - HTTP Status 401 with $this::INVALID_TOKEN message 
     *      if authentication token not valid.
     *  - HTTP Status 422 with $this::INVALID_INPUT message 
     *      and list of errors if input data was invalid.
     *  - HTTP Status 500 with $this::APPLICATION_ERROR message  
     *      and additional detailed message if route could not get
     *      returned or something else unexpected occured. (May be because 
     *      no route is possible, or a location can not be found.)
     **/
    public function addTrip(Request $request){
        $user = auth('api')->user();
        
        //Do authorization
        if(!$user) {
            return $this->returnErrorResponse(401, $this::INVALID_TOKEN, $this::INVALID_TOKEN);
        } 
        
        //Get all POST parameters and validate them
        $input=$this->formatInput($request->post());
        $validator = $this->getValidatorForRouteParameters($input);
        if($validator->fails()){
            return $this->returnErrorResponse(422, $this::INVALID_INPUT, $validator->errors());
        }
        
        //Try getting routing info, can create error responses
        $routeInfoOrError = $this->tryAndGetRoutingInfos($input, $user);
        
        //Check if it returned an array with information or array with error
        if(!is_array($routeInfoOrError) || isset($routeInfoOrError['error'])){
            //It is an error response
            return $routeInfoOrError;
        }

        //Otherwise, it is an array with information
        $routeInfo=$routeInfoOrError;

        //If here, means no error, add trip to database
        $newlyAddedTripOrError = $this->addTripToDB($routeInfo, $input, $user); //returns id of trip

        //Check if it returned an array with information or array with error
        if(isset($newlyAddedTripOrError['error'])){
            //It is an error response
            return $newlyAddedTripOrError;
        }
        
        $newlyAddedTrip = $newlyAddedTripOrError;
        
        $newlyAddedTripArray = $this->buildTripArrayFromDatabase($newlyAddedTrip); 
        return response()->json($newlyAddedTripArray, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8']);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //HELPER FUNCTIONS : VALIDATION
    ////////////////////////////////////////////////////////////////////////////
    
    //Convert all string keys and string values to lower-case for future comparison
    //and convert accepted term 'publicTransport' to app-term like 'transit'
    private function formatInput($input) {
        //make sure every key is lowercase
        foreach(array_keys($input) as $key){
            //engine have string value
            if($key=='engine'||strtolower($key)=='engine'){
                $input['engine']=strtolower($input['engine']);
            } 
            
            //mode have string value and might be publictransport instead of transit
            else if ($key=='mode'||strtolower($key)=='mode'){
                if(strtolower($input[$key])=='publictransport'){
                    $input['mode']='transit';
                } else {
                    $input['mode']=strtolower($input[$key]);
                }
            } 
            
            //Other keys doesnt have string values
            else {
                $input[strtolower($key)]=$input[$key];
            }
        }
        return $input;
    }
    
    //Create validator
    private function getValidatorForRouteParameters($input) {
        //Create validation rules
        $latitudeRule='required|min:'.Constants::LATITUDE_MIN_VALUE.
                        '|max:'.Constants::LATITUDE_MAX_VALUE;
        $longitudeRule='required|min:'.Constants::LONGITUDE_MIN_VALUE.
                        '|max:'.Constants::LONGITUDE_MAX_VALUE;
        
        //Make sure the comparision is case-insensitive
        $modeRule=['required', Rule::in(Constants::TRANSPORTATION_MODES_LOWERCASE)];
        
        
        //Make sure the comparision is case-insensitive
        $engineRule = ['sometimes', Rule::in(Constants::VEHICLE_TYPES_LOWERCASE)];

        
        //Validate 
        $validator = Validator::make($input, [
            'fromlatitude'=> $latitudeRule,
            'fromlongitude'=> $longitudeRule,
            'tolatitude'=> $latitudeRule,
            'tolongitude'=> $longitudeRule,
            'mode'=> $modeRule,
            'engine'=> $engineRule,
            'consumption'=> 'sometimes|min:0']);
        
        return $validator;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //HELPER FUNCTIONS 
    ////////////////////////////////////////////////////////////////////////////

    
    /**
     * Try to get route infous, may create error response
     * Returns an array with mandatory indexes:
     * ['routingInfos'] => Which hold the array returned by the GeoRouteRepository $routingInfos;
     * ['mode'] => trip's transportation mode
     *  
     * and optional indexes:
     *   ['engine']=> the trip's engine if the mode is car-related 
     *   ['consumption']=> the trip's engine's consumption if it has one
    **/
    private function tryAndGetRoutingInfos($input, $user){
        
        /*If start and destination are the same location, return error*/
        if($input['fromlatitude'] == $input['tolatitude']
        && $input['fromlongitude'] == $input['tolongitude']){
            return $this->returnErrorResponse(422, $this::INVALID_INPUT, 
                    '\'from\' and \'to\' parameters refer to the same location');
        }
        
        try {
            //If mode is not car-related
            if(!in_array($input['mode'], 
                            Constants::TRANSPORTATION_MODES_LOWERCASE_CAR)){
                $routingInfos = $this->routeRepository->getRouteBy(
                        $input['mode'],
                        $input['fromlatitude'], 
                        $input['fromlongitude'], 
                        $input['tolatitude'], 
                        $input['tolongitude']);
                $extendedRoutingInfos = [];
                $extendedRoutingInfos['routingInfos'] = $routingInfos;
                $extendedRoutingInfos['mode'] = $input['mode'];
            } 
            
            //If mode is car-related, we need to have an engine to calculate route
            else {
                                
                //If an engine have been specified in request
                if (isset($input['engine'])){
                    //If engine is not of valid type, return error.
                    if(!in_array($input['engine'], Constants::VEHICLE_TYPES_LOWERCASE)){
                        return $this->returnErrorResponse(422, $this::INVALID_INPUT, 
                                '\'engine\' parameter is not of invalid type');
                    }
                    
                    //If engine is electric, no need for consumption field, get route infos
                    if(in_array($input['engine'], Constants::VEHICLE_TYPES_LOWERCASE_NO_CONSUMPTION)){
                        $routingInfos = $this->routeRepository->getRouteByCar(
                            $input['fromlatitude'], 
                            $input['fromlongitude'], 
                            $input['tolatitude'], 
                            $input['tolongitude'],
                            $input['engine']
                        );
                        $extendedRoutingInfos = [];
                        $extendedRoutingInfos['routingInfos'] = $routingInfos;
                        $extendedRoutingInfos['mode'] = $input['mode'];
                        $extendedRoutingInfos['engine']=$input['engine'];
                        $extendedRoutingInfos['consumption']=0; //0 for electric cars!
                    } else {
                        /*If engine is not 'electric' and no consumption have 
                        been specified, return error.*/
                        if(!isset($input['consumption'])){
                            return $this->returnErrorResponse(422, 
                            $this::INVALID_INPUT, 
                            'if a \'engine\' parameter is specified and is not of type \'electric\', a \'consumption\' field must be specified too.'
                            );
                        }
                        
                        $inputConsumption = floatval($input['consumption']); //will have 0 if not valid float
                        
                        /*If consumption was not holding a float or was holding 
                        a value smaller or equal to 0, return error.*/
                        if(round($inputConsumption, 2) <= 0.0){
                            return $this->returnErrorResponse(422, 
                            $this::INVALID_INPUT, 
                            'if the \'engine\' parameter is specified and is not of type \'electric\', a \'consumption\' field must be a number higher than 0.'
                            );
                        }
                        
                        /*Now that we have engine and consumption validated,
                        get routing info*/
                        $routingInfos = $this->routeRepository->getRouteByCar(
                            $input['fromlatitude'], 
                            $input['fromlongitude'], 
                            $input['tolatitude'], 
                            $input['tolongitude'],
                            $input['engine'],
                            $inputConsumption
                        );
                        
                        $extendedRoutingInfos = [];
                        $extendedRoutingInfos['routingInfos'] = $routingInfos;
                        $extendedRoutingInfos['mode'] = $input['mode'];
                        $extendedRoutingInfos['engine']=$input['engine'];
                        $extendedRoutingInfos['consumption']=$input['consumption'];
                    }
                } 
                
                //If no engine have been specified, make sure there is a registered engine for this user
                else {
                   $userVehicle=$user->userstat->vehicle;
                   
                   //If no car is registered 
                    if(!isset($userVehicle) || is_null($userVehicle)){
                        return $this->returnErrorResponse(422, 
                             $this::INVALID_INPUT, 
                             'The user doesn\'t have a registered car. The \'engine\' parameter must be specified.');
                    } else {
                        $userEngine=(VehicleType::where('id', $userVehicle->vehicle_type_id)->get())[0]->type;
                        $userEngineAvgConsumption=$userVehicle->avg_consumption;

                        
                        if(!isset($userEngine, $userEngineAvgConsumption) || is_null($userEngine) || is_null($userEngineAvgConsumption)) {
                            return $this->returnErrorResponse(422, 
                             $this::INVALID_INPUT, 
                             'The user doesn\'t have a registered car. The \'engine\' parameter must be specified.');                            
                        }
                        
                        //Now that we are here, everything is validated and we can query the route
                        $routingInfos = $this->routeRepository->getRouteByCar(
                            $input['fromlatitude'], 
                            $input['fromlongitude'], 
                            $input['tolatitude'], 
                            $input['tolongitude'],
                            $userEngine,
                            $userEngineAvgConsumption);
                        $extendedRoutingInfos = [];
                        $extendedRoutingInfos['routingInfos'] = $routingInfos;
                        $extendedRoutingInfos['mode'] = $input['mode'];
                        $extendedRoutingInfos['engine']=$userEngine;
                        $extendedRoutingInfos['consumption']=$userEngineAvgConsumption;
                    }
                }
            } 
        } 
        
        //If invalid argument was detected here, return 422 with error.
        catch (\InvalidArgumentException $e){
            return $this->returnErrorResponse(422, $this::INVALID_INPUT, $e->getMessage());
        } 
        //Ir location or route is not findable, return 500 with detailed message.
        catch (\UnexpectedValueException $e){
            return $this->returnErrorResponse(500, $this::APPLICATION_ERROR, $e->getMessage());
        }
        
        //If here, means it was successfull, build JSON
        return $extendedRoutingInfos;
    }
    
    
    /**
     * Add trip to database, may create error responses
     **/
    private function addTripToDB($extendedRoutingInfos, $input, $user) {
		if(!isset($extendedRoutingInfos, $input, $user)){
			throw new Exception('params must be set');
		}
		
        //Get or create the start location
        $fromLocation = Location::firstOrCreate([
            'latitude' => $input['fromlatitude'],
            'longitude' => $input['fromlongitude']
        ]);
		
        
        //Get or create the end location
        $toLocation = Location::firstOrCreate([
            'latitude' => $input['tolatitude'],
            'longitude' => $input['tolongitude']
        ]);

		//Get transport mode
		//Transportations are stored with first letter as capital
		$transportation_mode = Transportation::where('transportation_method', 
		                            '=', $extendedRoutingInfos['mode'])
		                            ->get()[0];

		
		if(is_null($transportation_mode)){
			return $this->returnErrorResponse(500, $this::APPLICATION_ERROR, 
			'Unable to process demand');
		}
		
		//Get distance and time from routeRepository
		$routeInfoFromRepo = $extendedRoutingInfos['routingInfos'];
		$distance=$routeInfoFromRepo[$this->routeRepository::ROUTE_ARRAY_DISTANCE]/1000;
		$time=$routeInfoFromRepo[$this->routeRepository::ROUTE_ARRAY_TRAVEL_TIME];
		
		
		//If the mode is not car related
		if(!in_array($extendedRoutingInfos['mode'], Constants::TRANSPORTATION_MODES_LOWERCASE_CAR)){
		    //Calculate emission for public transit
		    if(strcasecmp($transportation_mode->transportation_method, 'transit')==0){
                $emissions = $distance / 1000.0 * Constants::TRANSIT_EMISSIONS_KG_PER_KM;
            } else {
                $emissions = 0;
            }
		    
		    //Create trip
			$trip = $user->trips()->create([
                            'start_location_id'=>$fromLocation->id, 
                            'destination_location_id'=>$toLocation->id, 
							 'transport_id'=>$transportation_mode->id,
                            'travel_duration'=>$time,
                            'travel_distance_km'=>$distance, 
                            'emission_vol'=>$emissions
                            ]);
		} 
		
		//If the mode is car-related
		else {
    		//Get emission from routeRepository
    		$emissions=$routeInfoFromRepo[$this->routeRepository::ROUTE_CAR_ARRAY_EMISSION];
    		
    		//Readjust emissions if carpool
            if(strcasecmp($transportation_mode->transportation_method, 'carpool')==0){
                $emissions = $emissions/Constants::CARPOOL_EMISSION_DIVISION_FACTOR;
            }
            
    		//get vehicle type id
    		$tripVehicleType = VehicleType::where('type', '=', $extendedRoutingInfos['engine'])
    		                        ->get()[0];
    		
    		if(is_null($tripVehicleType)){
    			return $this->returnErrorResponse(500, $this::APPLICATION_ERROR, 
    			'Unable to process demand');
    		}
    		
    		//get or create vehicle
    		$tripVehicle = Vehicle::firstOrCreate([
                'vehicle_type_id' => $tripVehicleType->id,
                'avg_consumption' => $extendedRoutingInfos['consumption']
            ]);
    		
    		
            //create trip
    		$trip = $user->trips()->create([ 
                                'start_location_id'=>$fromLocation->id, 
                                'destination_location_id'=>$toLocation->id, 
    							 'transport_id'=>$transportation_mode->id,
    							 'vehicle_id'=>$tripVehicle->id,
                                'travel_duration'=>$time,
                                'travel_distance_km'=>$distance, 
                                'emission_vol'=>$emissions
                                ]);
		}
		//update user stats
		ControllersHelper::updateUserStats($user, $distance, $emissions);
		
        return $trip;
    }
    

    
    ///////////////////////////////////////////////////////////////////////////
    //HELPER FUNCTIONS : Array and Response Building
    ////////////////////////////////////////////////////////////////////////////
    
    //Builds an array holding information about a trip retrieved from database
    private function buildTripArrayFromDatabase($trip) : array {
        $result=[];
        $result['id'] = $trip['id'];
                
        $fromLocation = $this->getTrip_StartLocation($trip['id']);
        $result['from'] = ['latitude'=>$fromLocation->latitude,
                            'longitude'=>$fromLocation->longitude];
                                    
        $toLocation = $this->getTrip_DestinationLocation($trip['id']);
        $result['to'] = ['latitude'=>$toLocation->latitude,
                         'longitude'=>$toLocation->longitude];

        $result['mode'] = Transportation::where('id', '=', $trip['transport_id'])
		                            ->get()[0]->transportation_method;
        
        //Add 'engine' field if needed and if present
        if(in_array(strtolower($result['mode']), Constants::TRANSPORTATION_MODES_LOWERCASE_CAR) ){
            if($trip->vehicle_id != null){
                $vehicle = $trip->vehicle;
                $vehicleType = (VehicleType::where('id', $vehicle->vehicle_type_id)
                                ->get())[0]->type;
                $result['engine'] = $vehicleType;
            }
        }
        $result['distance'] = $trip['travel_distance_km'];
        $result['traveltime'] = $trip['travel_duration'];
        $result['co2emissions'] = $trip['emission_vol'];
        $result['created_at'] = date("Y-m-d H:i", strtotime($trip['created_at']));
        return $result;
    }
  
    //Return response with error http code and error message
    private function returnErrorResponse($httpCode, $message, $errorMessageOrList){
        if(!isset($httpCode, $message, $errorMessageOrList)){
            throw new Exception('params need to be set');
        }
        $responseArray = ['error_type'=> $message, 'errors'=> $errorMessageOrList];
        return response()->json($responseArray, $httpCode);
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    //HELPER FUNCTIONS: DB QUERY 
    ////////////////////////////////////////////////////////////////////////////
    
    private function getTrip_StartLocation(string $tripId){
        $fromLocations = DB::table('locations')
                        ->join('trips', 'locations.id', '=', 'start_location_id')
                        ->select('locations.latitude', 'locations.longitude')
                        ->where('trips.id', '=', $tripId)->get();
        return $fromLocations[0];
    }
    
    private function getTrip_DestinationLocation(string $tripId){
        $toLocations = DB::table('locations')
                        ->join('trips', 'locations.id', '=', 'destination_location_id')
                        ->select('locations.latitude', 'locations.longitude')
                        ->where('trips.id', '=', $tripId)->get();
        return $toLocations[0];
    }
}