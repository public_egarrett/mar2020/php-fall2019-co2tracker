<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\UserStat;
use App\Location;
use App\Vehicle;
use App\Exceptions\NoLocationFoundException;
use App\Exceptions\TooManyLocationsException;
use App\Repositories\GeoRouteRepository;
use App\Constants;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


use RangeException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'avg_consumption' => ['required'],
            'fuel_type' => ['required'],
            'home-streetnum' => ['required', 'numeric'],
            'home-street' => ['required'],
            'home-city' => ['required'],
            'home-country' => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $sanitizedAddress = [
            'num' => rawurlencode(htmlentities($data['home-streetnum'])), 
            'street' => rawurlencode(htmlentities($data['home-street'])), 
            'city' => rawurlencode(htmlentities($data['home-city'])),
            'country' => rawurlencode(htmlentities($data['home-country']))
        ];
        
        $repository = new GeoRouteRepository();
        try {
            $coords = $repository->getGeocode($sanitizedAddress['num'], $sanitizedAddress['street'], $sanitizedAddress['city'], $sanitizedAddress['country']);
        } catch (\Exception $e){
             throw new NoLocationFoundException("User home address getGeocode call was unsuccessful"); 
        }
        
        if(!is_array($coords)){
            throw new NoLocationFoundException("User home address getGeocode call didnt return an array");
        }
        //this means more than 1 address could correspond
        else if( array_key_exists($repository::GEO_CODE_ARRAY_TYPE_ADDRESSES, $coords)){
            throw new TooManyLocationsException("User home address input could correspond to more than 1 address");
        } else if(!array_key_exists('longitude', $coords)
                    || !array_key_exists('latitude', $coords)){
            throw new NoLocationFoundException("User home address getGeocode call was unsuccessful"); 
        }
        
        $location = Location::firstOrCreate([
                'longitude' => $coords['longitude'],
                'latitude' => $coords['latitude']
            ]);
            
        if ($data['fuel_type'] != '0') {
            $vehicle = Vehicle::create([
                    'vehicle_type_id' => (int) $data['fuel_type'],
                    'avg_consumption' => (double) $data['avg_consumption'],
            ]);
        } else {
            $vehicle = null;
        }
        
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        
        $userstat = $user->userstat()->create([
            'home_location_id' => $location->id,
            'avg_consumption' => (double) $data['avg_consumption'],
            'total_commute_km' => 0,
            'yearly_emissions' => 0,
            'offset' => 0
        ]);
        
        if ($vehicle != null) {
            $userstat->vehicle_id = $vehicle->id;
            $userstat->save();
        }
        
        return $user;
    }
    
}