# CO2 Tracker Web Page

Demo users: 
- email: demo@co2tracker.com password: "password"
- email: demo2@co2tracker.com password: "password"

# CO2 Tracker Web API

Co2Tracker is a PHP-written web API that allows you to track your trips and CO2 emissions.

### Registration
-------------
URL : https://co2trackerbyec.herokuapp.com/register

### Authentication
-------------
The  Co2Tracker API uses bearer tokens to authenticate requests.
You can request a bearer token if you are already a registered user.
  - URL : https://co2trackerbyec.herokuapp.com/api/auth/login 
  - Method : POST
  - POST Parameters : 
```json 
  {
    "email":"your@email.com", 
    "password":"your_password"
  }
```
  - Curl Command:
```bash
curl -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"email": "your@email.com", "password":"your_password"}' https://co2trackerbyec.herokuapp.com/api/auth/login
```
  -  Success Response :
```json
 {
    "access_token":"longtoken",
    "token_type":"bearer",
    "expires_in":3600
}
```

### Get User's Trip Log
-------------

  - URL : https://co2trackerbyec.herokuapp.com/api/v1/alltrips
  - Method : GET
  - Header : Bearer Token
  - Parameters : None
  - Curl Command:
```bash
curl -X GET -H "Authorization: Bearer bearer_token_here"  https://co2trackerbyec.herokuapp.com/api/v1/alltrips
```
  -  Success Response :
```json
[
  {
   "id":1,
   "from":{"latitude":"45.490131","longitude":"-73.589383"},
   "to":{"latitude":"45.531519","longitude":"-73.639131"},
   "mode":"transit",
   "distance":"11.708",
   "traveltime":"3197",
   "co2emissions":"0.05409096",
   "created_at":"2019-12-04 18:24"
  },...
]
```
Array might be empty if user doesn't have any trips yet.

### Get Trip Info
-------------
  - URL : https://co2trackerbyec.herokuapp.com/api/v1/tripinfo
  - Method : GET
  - Header : Bearer Token
  - GET Parameters :
```json
   ?tolatitude=45.490131&tolongitude=-73.589383&fromlatitude=45.531519&fromlongitude=-73.639131&mode=transit
```
  - Curl Command:
```bash
curl -X GET -H "Authorization: Bearer bearer_token_here" "https://co2trackerbyec.herokuapp.com/api/v1/alltrips?tolatitude=45.544898&tolongitude=-73.632378&fromlatitude=45.637071&fromlongitude=-73.495339&mode=transit"
```
  -  Success Response :
```json
{
   "distance":"11.708",
   "traveltime":"3197",
   "co2emissions":"0.05409096"
}
```


### Add Trip to User's Log
-------------
  - URL : https://co2trackerbyec.herokuapp.com/api/v1/addtrip
  - Method : POST
  - Header : Bearer Token
  - POST Parameters :
```json
   ?tolatitude=45.490131&tolongitude=-73.589383&fromlatitude=45.531519
   &fromlongitude=-73.639131&mode=transit
```
  - Curl Command:
```bash
curl -X POST -H "Accept: application/json" -H "Authorization: Bearer bearer_token_here" -H "Content-Type: application/json" http://co2trackerbyec.herokuapp.com/api/v1/addtrip -d '{"fromlatitude":45.544898, "fromlongitude":-73.632378,"tolatitude":45.637071,  "tolongitude":-73.495339,"mode":"publicTransport"}'```
  -  Success Response :
```json
{
   "id":1,
   "from":{"latitude":"45.544898","longitude":"-73.632378"},
   "to":{"latitude":"45.637071","longitude":"-73.495339"},
   "mode":"transit",
   "distance":"11.708",
   "traveltime":"3197",
   "co2emissions":"0.05409096",
   "created_at":"2019-12-04 18:24"
}
```

### Parameters
| Name | Rule|
| ------ | ------ |
| "tolatitude" | Must be a number between -90 and 90. |
| "tolongitude" | Must be a number between -180 and 180. |
| "fromlatitude" | Must be a number between -90 and 90. |
| "fromlongitude " | Must be a number between -180 and 180. |
| "mode" | Must be in : "car", "carpool", "transit" or "publictransport", "bike", "walk" |
| "engine" | Must be in : "gasoline", "diesel" or "electric". Not required if "mode" is not "car" or "carpool" |
| "consumption" | Must be a positive number. Not required if "mode" is not "car" or "carpool" or if "engine" is "electric" |

### Return Values
| Name | Unit |
| ------ | ------ |
| "distance" | Kilometers |
| "traveltime" | Seconds |
| "co2Emissions" | Kilograms |

### HTTP Codes
-------------
| HTTP Code | README |
| ------ | ------ |
| 200 | OK. |
| 401 | Unauthorized. No valid bearer token provided. |
| 422 | Invalid parameters. |
| 500 | Application Error. |

### Errors
-------------
Error Response Format :
```json
{ 
	"error_type" : "error_type", 
	"errors" : "error_or_list_of_errors"
}
```

Where *error_type* can be :
* "invalid_token"
* "invalid_parameters"
* "application_error"
If its an application_error, *errors* can be 'NoRouteFound', 'WaypointNotFound' or other!